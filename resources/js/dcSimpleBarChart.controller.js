(function () {
    'use strict';
    
    function DCSimpleBarChartController($scope, $q, $timeout) {
        $scope.DECISYON.target.registerDataConnector(function (requestor) {
            var deferer = $q.defer();
            
            $timeout(function(){
                deferer.resolve([
                    {
                      chartData: [[102,80,63,127,102,77,57,71,106,78,129],[103,55,54,103,89,56,74,52,67,110,93],[128,119,71,74,98,87,114,67,67,62,104],[92,64,88,121,61,55,115,110,78,129,119],[102,83,106,58,102,62,58,67,75,113,120],[68,129,94,58,53,117,96,87,97,70,108],[109,109,104,104,121,86,97,51,73,100,74],[112,57,53,122,128,120,56,94,72,110,102]],
                      legendLabels: ['A','B','C','D','E','F','G','H','I','J','K','L']
                    }
                ]);                
            },10); 
            
            return {
                data : deferer.promise
            };
        });
        
    }
    
    DCSimpleBarChartController.$inject = ['$scope', '$q', '$timeout'];
    DECISYON.ng.register.controller('dcSimpleBarChartCtrl', DCSimpleBarChartController);
    
}());