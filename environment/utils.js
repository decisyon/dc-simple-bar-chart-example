(function( $ ) {
		var loop = null;
		var elements = [];
	
		$.fn.resizeElement = function(callBack) {
			if (elements.indexOf(this) < 0) {
				elements.push(this);
				this.data("resizeElement", { w: this.width(), h: this.height(),  call: callBack} );
			}
			
			if (!isDefined(loop)) {
				loop = setTimeout(function() {
					loopy();
				});
			}
	
			return this;
		};
		
		function loopy() {
			for (var i=elements.length-1; i>=0; i--){
				var $elem = elements[i];
				if($elem.closest('body').length < 1) {
					   elements.splice(i, 1);
					   continue;

				}
				
				var width  = $elem.width();
		        var height = $elem.height();
		        var data   = $elem.data("resizeElement" );
		        
				// If element size has changed since the last time, update the element
				// data store and trigger the 'resize' event.
		        if ( width !== data.w || height !== data.h ) {
		        	$elem.data("resizeElement", { w: width, h: height,  call: data.call} );
		        	data.call();
		        }
			}
		      
		     if (elements.length) {
		    	 loop = setTimeout(function() {
		    		 loopy();
		    	 }, 250);
		     }
		     else {
		    	 loop = null;
		     }
		}
}( jQuery ));

/**
 * Author : Pagano Francesco
 * Extend DatePicker : add new callback for 'onShow' event;
 * This event is launched when datepicker is shown
 */
(function( $ ) {
	return false;
    $.datepicker._updateDatepickerRoot = $.datepicker._updateDatepicker;
    $.datepicker._updateDatepicker = function(instance) {
        $.datepicker._updateDatepickerRoot(instance);
        var onShow = this._get(instance, 'onShow');
        if (onShow){
        	onShow(instance.input[0], instance.dpDiv[0]);
        }
    };
}( jQuery ));

/**
 * END - Gestione del resize, da eliminare dopo aver risolto il problema della chatDiscussion
 */

/**
 * Funzione per la visualizzazione dei tag sia per Social document sia per Task
 */
function showDivTip(currentNode, idDivContent, title) {
	var opt = {
			show: {
				solo: true
			}
	};
	
	createJQTooltipWithSimpleHTML($(currentNode), title, $("#"+idDivContent).get(0).innerHTML, opt);
}

// Return all ComputedStyle or CurrentStyle or Style of an element
$.fn.getStyleObject = function(){
	var dom = this.get(0);
	var style;
	var returns = {};
	if(window.getComputedStyle){
		var camelize = function(a,b){
			return b.toUpperCase();
		};
		style = window.getComputedStyle(dom, null);
		for(var i = 0, l = style.length; i < l; i++){
			var prop = style[i];
			var camel = prop.replace(/\-([a-z])/, camelize);
			var val = style.getPropertyValue(prop);
			returns[camel] = val;
		}
		return returns;
	}
	if(style = dom.currentStyle){
		for(var prop in style){
			returns[prop] = style[prop];
		};
		return returns;
	}
	if(style = dom.style){
		for(var prop in style){
			if(typeof style[prop] != 'function'){
				returns[prop] = style[prop];
			}
		}
		return returns;
	}
	return returns;
};

xx = 20;
yy = 70;

function setVisible(obj)
{
	obj = document.getElementById(obj);
	if(obj == null)
		return;
	if ($(obj).css("visibility")=="visible")
		$(obj).css({"visibility":"hidden"});
	else
		$(obj).css({"visibility":"visible"});
}

/************** Da convertire ****************/
//aumento alfa
function enlight(i, id) {
	document.getElementById(id).style.filter = "progid:DXImageTransform.Microsoft.Alpha(Opacity="
			+ i + ",Style=0 )";
}

// aumento bordo shadow
function shadow(i, id) {
	document.getElementById(id).style.filter = "progid:DXImageTransform.Microsoft.Shadow(color='gray', Direction=135, Strength="
			+ i + ")";
}

// transizione shadow
function shadowTransition(id, maxVal) {
	for (var i = 0; i <= 20; i++) {
		if (i <= maxVal)
			self.setTimeout("shadow(" + i + ",'" + id + "')", (i + 5) * 20);
	}
}
/************** End Da convertire ****************/

/*
 * converte da decimale (0..255) a esadecimale (stringa a due caratteri) @author
 * Costantini Simone 15/gen/08 10:41:18 Copyright ©2006 Decisyon S.r.l.
 */
function DecToHex(n) {
	if (n > 255)
		n = 255;

	hex = n.toString(16);
	if (hex.length == 1)
		hex = "0" + hex; /*
							 * aggiunge lo zero davanti se è un numero con una
							 * cifra sola
							 */
	return hex.toUpperCase();
}

/*
 * converte da stringa esadecimale a numero decimale @author Costantini Simone
 * 15/gen/08 10:41:18 Copyright ©2006 Decisyon S.r.l.
 */
function HexToDec(s) {
	return parseInt(s, 16);
}

/*
 * a partire da un colore esadecimale restituisce un colore alterato del valore
 * rgb specificato. @author Costantini Simone 15/gen/08 10:41:18 Copyright ©2006
 * Decisyon S.r.l.
 */
function colorCorrection(exColor, rgbToRemove) {
	var output="";
	try {
		var red = HexToDec(exColor.substring(1, 3));
		var green = HexToDec(exColor.substring(3, 5));
		var blue = HexToDec(exColor.substring(5, 7));

		if (red > rgbToRemove)
			red = red - rgbToRemove;
		else
			red = 0;
		if (green > rgbToRemove)
			green = green - rgbToRemove;
		else
			green = 0;
		if (blue > rgbToRemove)
			blue = blue - rgbToRemove;
		else
			blue = 0;
		output = "#" + DecToHex(red) + DecToHex(green) + DecToHex(blue);
	} catch (e) {
	}
	return output;
}


/* verifica se una stringa finisce con una specifica sequenza di caratteri */
String.prototype.endsWith = function(str) {return (this.match(str+"$")==str);};

/* elimina gli sopazi bianchi all'inizio e alla fine di una stringa */
String.prototype.trim = function(){return (this.replace(/^[\s\xA0]+/, "").replace(/[\s\xA0]+$/, ""));};

/* verifica se una stringa inizia con una specifica sequenza di caratteri */
String.prototype.startsWith = function(str) {return (this.match("^"+str)==str);};

/*************************************************************************
* La funzione AddClassName aggiunge una classe all'attributo classe di un
* elemento del DOM. Argomenti (elemento, nome della classe da aggiungere).
**************************************************************************/
function AddClassName(objElement, strClass){
	// Viene recuperato nuovamente perchè può essere stato riscritto da un innerHTML
	var newInstance = document.getElementById(objElement.id);
	$(newInstance).addClass(strClass);
}

/*************************************************************************
* La funzione HasClassName ritorna un booleano per indicare se l'elemento 
* ha la classe specificata. Argomenti (elemento, nome della classe).
**************************************************************************/
function HasClassName(objElement, strClass){
	// Viene recuperato nuovamente perchè può essere stato riscritto da un innerHTML
	var newInstance = document.getElementById(objElement.id);
	return $(newInstance).hasClass(strClass);
}
/* ****************************************************************************
** Restituisce la classe in input se esistente.
**************************************************************************** */
function getStyleClass (className) 
{ 
	if (document.styleSheets.length < 1) { 
		return null;		  
	}
	var cssRules='';
	if (document.styleSheets[0].cssRules) { 
		cssRules = 'cssRules';		    
	} else { 
		cssRules = 'rules';		    
	} 
	for (var s = 0; s < document.styleSheets.length; s++) { 		       
		for (var r = 0; r < document.styleSheets[s][cssRules].length; r++) { 		          
			if (document.styleSheets[s][cssRules][r].selectorText == '.' + className) { 		                
				return document.styleSheets[s][cssRules][r]; 		         
			} 
		}		   
	} 		   
	return null; 
}               
	
/*************************************************************************
* La funzione RemoveClassName rimuove una classe all'attributo classe di un
* elemento del DOM. Argomenti (elemento, nome della classe da rimuovere).
**************************************************************************/
function RemoveClassName(objElement, strClass){
	// Viene recuperato nuovamente perchè può essere stato riscritto da un innerHTML
	var newInstance = document.getElementById(objElement.id);
	$(newInstance).removeClass(strClass);
}

//==========================================================================
// sostituisce tutte le occorrenze "strA" con la variabile "strB" all'interno 
// della stringa â€œtextâ€?
// ==========================================================================
function replaceAll(text, strA, strB)
{
    return (text && text != null && text != "") ? text.replace( new RegExp(strA,"g"), strB ) : text;    
}   
	
var jsErrorDetail ='';
/**
 * Memorizza, sul server un determinato errore Javascript
 * @param obj
 * @return
 * Simone Costantini 13/ott/2009
 */
function sendJavascriptError(jsException){
	var msg = escape(jsException);
	$.ajax({
		data: {revent:'LW_STORE_JS_ERROR',a0:msg, noShowError:'1'},
		success: function(data, textStatus, xhr) {}
	});
	jsErrorDetail ="";
}   

/**
 * Funzione standard di cattura errori js e scrittura nel log
 * Luigi Pagliaroli 13 Gennaio 2010
 */

//la riga da includere nelle jsp in cui attivare log errori
window.onerror = function(msg, url, linenumber){
	return logStackTrace(msg, url, linenumber);
};
//onerror = logStackTrace;

//nome utente e computer NON USATO PER PROBLEMI AUTORIZZATIVI
//var ax = new ActiveXObject("WScript.Network");
//var userName = ax.UserName;
//var computerName = ax.ComputerName;
 
 //funzione che estrae lo stacktrace dell'errore
 function printStackTrace() {
  var callstack = [];
  var isCallstackPopulated = false;
  try {
	i.dont.exist+=0; //doesn't exist- that's the point
  } catch(e) {
	if (e.stack) { //Firefox
	  var lines = e.stack.split("\n");
	  for (var i=0, len=lines.length; i<len; i++) {
		if (lines[i].match(/^\s*[A-Za-z0-9\-_\$]+\(/)) {
		  callstack.push(lines[i]);
		}
	  }
	  //Remove call to printStackTrace()
	  callstack.shift();
	  isCallstackPopulated = true;
	}
	else if (window.opera && e.message) { //Opera
	  var lines = e.message.split("\n");
	  for (var i=0, len=lines.length; i<len; i++) {
		if (lines[i].match(/^\s*[A-Za-z0-9\-_\$]+\(/)) {
		  var entry = lines[i];
		  //Append next line also since it has the file info
		  if (lines[i+1]) {
			entry += " at " + lines[i+1];
			i++;
		  }
		  callstack.push(entry);
		}
	  }
	  //Remove call to printStackTrace()
	  callstack.shift();
	  isCallstackPopulated = true;
	}
  }
  if (!isCallstackPopulated) { //IE and Safari
	/* var currentFunction = arguments.callee.caller;
	while (currentFunction) {
	  var fn = currentFunction.toString();
	  var fname = fn.substring(fn.indexOf("function") + 8, fn.indexOf("(")) || "anonymous";
	  callstack.push(fname);
	  currentFunction = currentFunction.caller;
	} */
	var currentFunction = arguments.callee.caller;
	while (currentFunction) {
		var fn = currentFunction.toString();
		//If we can't get the function name set to "anonymous"
		var fname = fn.substring(fn.indexOf("function") + 8, fn.indexOf("(")) || "anonymous";
		// callstack.push(fname);

		var args = currentFunction.arguments.length + " args (";
		for (var i=0; i<currentFunction.arguments.length; i++){
			args += i + "=" + currentFunction.arguments[i] + "\n )";		
		}
		callstack.push(fname + " " + args);

		currentFunction = currentFunction.caller;
	}
  }
  return(callstack.join("\n"));
}
 
 

 function logStackTrace(msg,url,line){
	 var errFullMsg="";
	 var isThirdPartiesJSErrors = isThirdPartiesJSError(msg);
	 var errMsg = (isThirdPartiesJSErrors)? ' **THIRD PARTIES JS ERROR** '+msg: msg;
	 //ERROR INFORMATIONS
	 errFullMsg += getJSErrorInfos(errMsg, url, line);
	 
	 //STACKTRACE
	 errFullMsg += printStackTrace() + "\n\n";
	 
	 //NAVIGATOR INFORMATIONS
	 errFullMsg += getNavigatorInfos();
	 
	 //OUTPUT
	 sendJavascriptError(errFullMsg);
	 if (!isThirdPartiesJSErrors)
		 showJavascriptErrorIntoJQDialog(errFullMsg);
	 
	 //hideAllLoading();

	 return true;
 }
 
/**
 * Ritorna lo stack trace javascript se disponibile (su IE non disponibile)
 * 
 * @param e: oggetto Error di javascript
 * @returns: Stringa con lo stack trace
 */
function getStackTrace(e) {
	if (e.stack) {
		return e.stack;
	} else {
		return e + "";
	}
}
 
 /**
  * Stampa il tipo di rendering del browser
  * Simone Costantini 13/ott/2009
  * @return
  */
 function getRenderingEngine(){
	 var engine = null;
	 if (document.documentMode) // only IE8
	       engine = document.documentMode;
	return engine;
 }
 
/**
 * Luigi Pagliaroli 01/feb/2010
 * apre nel frame bottom i tab contenenti i log di decisyon
 */
try{
	var objBottomFrame = window.parent.document.getElementById("bottom");
	if(objBottomFrame && objBottomFrame.src==''){
		objBottomFrame.src='cassiopeaWeb/misc/msgWindow/msgWindow.jsp';
	}
}catch(e){
	//alert(e);
}

//-----------------------------------------------------------------------
// Gestione encode e decode in UTF-8
//-----------------------------------------------------------------------
var Utf8 = {    // public method for url encoding    
		encode : function (string) {        
			string = string.replace(/\r\n/g,"\n");        
			var utftext = "";        
			for (var n = 0; n < string.length; n++) {            
				var c = string.charCodeAt(n);  
				if (c == 43) {   //+
					utftext += " ";
				}else if (c < 128) {                
					utftext += String.fromCharCode(c);            
				}else if((c > 127) && (c < 2048)) {                
					if (c == 224){ //à
						utftext += "\u00e0";
					}else if (c == 242){ //ò
						utftext += "\u00f2";
					}else if (c == 232){ //è
						utftext += "\u00e8";
					}else if (c == 233){ //é
						utftext += "\u00e9";
					}else if (c == 249){ //ù
						utftext += "\u00f9";
					}else if (c == 236){ //ì
						utftext += "\u00ec";
					}else if (c == 163){ //£
						utftext += "\u00a3";
					}else if (c == 176){ //°
						utftext += "\u00b0";
					}else if (c == 200){ //È
						utftext += "\u00c8";
					}else if (c == 210){ //Ò
						utftext += "\u00d2";
					}else if (c == 192){ //À
						utftext += "\u00c0";
					}else if (c == 217){ //Ù
						utftext += "\u00d9";
					}else if (c == 204){ //Ì
						utftext += "\u00cc";
					}else if (c == 193){ //Á
						utftext += "\u00c1";
					}else if (c == 201){ //É
						utftext += "\u00c9";
					}else if (c == 205){ //Í
						utftext += "\u00cd";
					}else if (c == 211){ //Ó
						utftext += "\u00d3";
					}else if (c == 218){ //Ú
						utftext += "\u00da";
					}else{
						utftext += String.fromCharCode((c >> 6) | 192);                
						utftext += String.fromCharCode((c & 63) | 128);
					}
				}else if(c == 8364) {    //euro 
					utftext += "\u20AC";
				}else {                
					utftext += String.fromCharCode((c >> 12) | 224);                
					utftext += String.fromCharCode(((c >> 6) & 63) | 128);                
					utftext += String.fromCharCode((c & 63) | 128);            
				}        
			}        
			return utftext;    
		},    
		// public method for url decoding    
		decode : function (utftext) {        
			var string = "";        
			var i = 0;        
			var c = c1 = c2 = 0;        
			while ( i < utftext.length ) {            
				c = utftext.charCodeAt(i); 
				
				if (c < 128) {                
					string += String.fromCharCode(c);                
					i++;            
				}else if((c > 191) && (c < 224)) {                
					c2 = utftext.charCodeAt(i+1);                
					string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));                
					i += 2;            
				}else {                
					c2 = utftext.charCodeAt(i+1);                
					c3 = utftext.charCodeAt(i+2);                
					string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));                
					i += 3;            
				}        
			} 
			return string;
		}
};

function disableTotalInCombo(event, objTable) {
	var srcObj = getEventCell(event);
	if (srcObj.tagName != "INPUT") return;
	if (srcObj.name == "CATCHallValueID9999") return;
	var inps = objTable.getElementsByTagName("INPUT");
	if (srcObj.checked)
		for (var i=0;i<inps.length;i++)
			if (inps[i].name=="CATCHallValueID9999")
				inps[i].checked=false;
}

function JSFX_FloatDiv(divObj, verticalPos, orizzontalPos)
{
	var startX = 0,
	startY = 1;
	var ns = (navigator.appName.indexOf("Netscape") != -1);
	var d = document;
	function ml(id)
	{
		var el=d.getElementById?d.getElementById(id):d.all?d.all[id]:d.layers[id];
		if(d.layers)el.style=el;
		el.sP=function(x,y){this.style.left=x;this.style.top=y;};
		if (orizzontalPos=="fromleft")
			el.x = startX;
		else
		{
			el.x = ns ? pageXOffset + innerWidth : document.body.scrollLeft + document.body.clientWidth;
			el.x -= startY;
		}
		if (verticalPos=="fromtop")
			el.y = startY;
		else
		{
			el.y = ns ? pageYOffset + innerHeight : document.body.scrollTop + document.body.clientHeight;
			el.y -= startY;
		}
		return el;
	}
	document.getElementById(divObj).stayTopLeft = function()
	{
		if (orizzontalPos=="fromleft"){
			var pX = ns ? pageXOffset : document.body.scrollLeft;
			ftlObj.x += (pX + startX - ftlObj.x)/8;
		}
		else
		{
			var pX = ns ? pageXOffset + innerWidth : document.body.scrollLeft + document.body.clientWidth;
			ftlObj.x += (pX - startX - ftlObj.x)/8;
		}
		if (verticalPos=="fromtop"){
			var pY = ns ? pageYOffset : document.body.scrollTop;
			ftlObj.y += (pY + startY - ftlObj.y)/8;
		}
		else
		{
			var pY = ns ? pageYOffset + innerHeight : document.body.scrollTop + document.body.clientHeight;
			ftlObj.y += (pY - startY - ftlObj.y)/8;
		}
		ftlObj.sP(ftlObj.x, ftlObj.y);
		fun = document.getElementById(divObj).stayTopLeft;
		setTimeout("fun()", 10);
	};
	ftlObj = ml(divObj);
	document.getElementById(divObj).stayTopLeft();
}

/* ****************************************************************************
** Restituisce la TD su cui è stato attivato l'evento
**************************************************************************** */
function getEventCell(event)  {
	try{
		if (!event)   
			return false;	
		
		var srcElem = event.target || event.srcElement || event.originalTarget; 
		return srcElem;
	}catch(e){
		alert('getEvent:' +e.name+' ' +e.message);
	}
} 	

/**
 * Funzione jQuery che permette la disabilitazione della selezione del testo.
 * Utilizzo:
 *   jQruery(selettore).disableSelection();
 */
	$.fn.disableSelection = function() {
	    return this.each(function() {           
	        $(this).attr('unselectable', 'on')
               .css({
                   '-moz-user-select':'none',
                   '-webkit-user-select':'none',
                   'user-select':'none'
               })
               .each(function() {
                   this.onselectstart = function() { return false; };
               });
	    });
	};

/**
 * Funzione jQuery che permette la riabilitazione della selezione del testo.
 * Utilizzo:
 *   jQruery(selettore).enableSelection();
 */
	$.fn.enableSelection = function() {
	    return this.each(function() {           
	        $(this).attr('unselectable', 'off')
               .css({
                   '-moz-user-select':'',
                   '-webkit-user-select':'',
                   'user-select':''
               })
               .each(function() {
                   this.onselectstart = function() { return true; };
               });
	    });
	};

	//Funzione jQuery che permette di controllasre se la selezione sia abilitata o meno
	$.fn.isSelectionEnabled = function() {
	    return $(this).attr('unselectable') != 'on';
	};
	
/**
 * Commentata perche l'istruzione this.before(s) elimina eventuali javascript esistenti nell'elemento "s"
 */
/*$.fn.outerHTML = function(s) {
	return (s)
		? this.before(s).remove()
		: $("<p>").append(this.eq(0).clone()).html();
};*/

/**************************************************************************************
 * Functions extracted by mainDetails.js (this file was removed)
**************************************************************************************/
var ie=document.all;
var nn6=document.getElementById&&!document.all;
var isdrag=false;
var x,y,z=0;
var dobj;

function movemouse(e)
{
  if (isdrag)
  {
    dobj.style.pixelLeft = nn6 ? tx + e.clientX - x : tx + event.clientX - x;
    dobj.style.pixelTop  = nn6 ? ty + e.clientY - y : ty + event.clientY - y;
    return false;
  }
  else
    return false;
};
/* ****************************************************
 * Funzione richiamata dalla selezione del modello
 * nella sezione Reports della pagina modele.jsp
 ***************************************************** */
function changeModelReportSection(modelName,modelDisplay,event){
	rootPage.stopPropagation(event);
	rootPage.toggleHeaderHeight(null, event, false);
    document.location.href=ENV_VARS.getValue('contextPath')+"/cassiopeaWeb/catalogue/mainCatalogue.jsp.cas?revent=1&setMainModel=1&csMLO="+modelName+"&setMainModel=1&container=10000000&contName=Root";

};


/* ****************************************************
 * Funzione richiamata dalla selezione del modello
 * nella sezione Dashboads della pagina modele.jsp
 ***************************************************** */
function changeModelDashboardSection(modelName,modelDisplay){
	if (typeof event=="undefined")
		event=null;
	rootPage.stopPropagation(event);
	rootPage.toggleHeaderHeight(null, event, false);
    document.location.href=ENV_VARS.getValue('contextPath')+"/cassiopeaWeb/dsh/dshMain.jsp.cas?revent=LOAD_DSH_TREE&setMainModel=1&csMLO="+modelName;
};

function loadDashboardInCenterIframe(modelName,modelDisplay){
	var $centralFrame = $("#centrale", $("#divCenter"));
	$centralFrame.attr("src", ENV_VARS.getValue('contextPath')+"/cassiopeaWeb/dsh/dshMain.jsp.cas?revent=LOAD_DSH_TREE&csMLO="+modelName);
};

/* ****************************************************
 * Shows a div with a shadow effect * 
 ***************************************************** */
function showDiv(name){
	document.getElementById(name+"Div").style.pixelTop=event.y+10;
	document.getElementById(name+"Div").style.pixelLeft=event.x-500;
	document.getElementById(name+"Div").style.visibility='visible';
	shadow(0,name);
	for(var i=0;i<=10;i++){
		window.setTimeout("enlight("+i*10+",'"+name+"')", i*40) ;
		window.setTimeout("shadow("+i*10+",'"+name+"')", (i+5)*40) ;
	}
};

/**
 * Aggiunge un metodo a jQuery che permette di controllare quando un attributo del css di un oggetto cambia
 * Tantalo Christian Alessandro
 */
jQuery.fn.onPropertyChange = function(cssId, fn, tm, timeout ) {
    return this.each(function(){
        var self = $(this);
        var oldVal = self.css(cssId);
        $(self).data(
            'propertyChange_timer',
            setInterval(function(){
                if (self.css(cssId) !== oldVal) {
                    fn.call(self, cssId, oldVal, self.css(cssId));
                    oldVal = self.css(cssId);
                }
            }, tm)
        );
        if (timeout>0)
        	setTimeout(function(){clearInterval($(this).data('propertyChange_timer'));fn.call(self, cssId, oldVal, self.css(cssId));}, timeout);
    });
    return self;
};
 
jQuery.fn.removePropertyChange = function() {
    return this.each(function(){
        clearInterval($(this).data('propertyChange_timer') );
    });
};
/* ****************************************************
 * Shows the JS error into the jquery dialog and 
 * sets the link to open a Trac ticket
 ***************************************************** */
function showJavascriptErrorIntoJQDialog(errMsg){
	if (false && ENV_VARS.isInTesting()){ //finestra di debug esplicita eccezione disattivata
		var titleTT = "title="+escape("[Cross browsing]");
		var messageTT = escape(errMsg);
		var params = "?"+titleTT+"&message="+messageTT;
		var linkTrac = "<br><br><button  class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only\" role=\"button\" onclick=\"rootPage.linkTrac('"+params+"');\"><span class=\"ui-button-text\">Apri TT</span></button>";
		JQAlertError('jqAlertError', 'test', unescape(replaceNewLineCharWithBRTag(messageTT))+linkTrac);
		setJQDialogOptions('jqAlertError', 'Error', '700', '400', -1, -1, true, true, true);
	}
};
/* ****************************************************
 * Replaces the new line char with a '<br/>' tag
 ***************************************************** */
function replaceNewLineCharWithBRTag(text){
	var re_nlchar="";
	if(text.indexOf('%0D%0A') > -1){
		re_nlchar = /%0D%0A/g ;
	}else if(text.indexOf('%0A') > -1){
		re_nlchar = /%0A/g ;
	}else if(text.indexOf('%0D') > -1){
		re_nlchar = /%0D/g ;
	}	
	return text.replace(re_nlchar,'<br/>');
};
/* ****************************************************
 * Returns the formatted JS error informations
 ***************************************************** */
function getJSErrorInfos(msg, url, line){
	return "Javascript error: " + msg + " in URL: " + url + " > " + "Line Number: " + line + "\n\n";
};
/* ****************************************************
 * Returns the navigator object informations
 ***************************************************** */
function getNavigatorInfos(){
	 
	 var x = navigator;
	 var errFullMsg = "CodeName=" + x.appCodeName + ", ";
	 
	 if (x.appMinorVersion!= "undefined")
		 errFullMsg += "MinorVersion=" + x.appMinorVersion + ", "; //only IE e Opera
	 
	 errFullMsg += "Name=" + x.appName + ", ";
	 errFullMsg += "Version=" + x.appVersion + "\n";
	 
	 var renderingEngine = getRenderingEngine();
	 if (renderingEngine!=null)
		 errFullMsg += "Rendering Engine= " + renderingEngine + "\n";
	 
	 errFullMsg += "CompatMode= " + document.compatMode + "\n";	 
	 errFullMsg += "UA=" + x.userAgent + "\n";
	
	return errFullMsg;
};
/**
 * Function to close the planning dialog for data actions by rootPage
 * @returns {Boolean}
 */
function closeDataActionsPage(){
	if(rootPage.jqDialogIframe && rootPage.jqDialogIframe.userInterface.$('#btnCancel').size()>0 && rootPage.jqDialogIframe.userInterface.isWebButtonEnabled('btnCancel')){
		rootPage.jqDialogIframe.userInterface.$('#btnCancel').click();
	}else{
		if(isPlanningDashboardReferenceWindow()){
			var referenceWindow = rootPage.jqDialogIframe.userInterface.centrale;
			if (referenceWindow.isWebButtonEnabled('btnCancel')){	
				referenceWindow.$('#btnCancel').click();
			}else {
				referenceWindow.closeDataActionsDialog();
			}
		}else if (rootPage.jqDialogIframe && rootPage.jqDialogIframe.userInterface ){
			rootPage.jqDialogIframe.userInterface.closeDataActionsDialog();
		}
	}
	return false;	
};
/**
 * Return if the reference window is the dashboard page
 * @returns
 */
function isDashboardReferenceWindow(){
	return (rootPage.centrale &&  rootPage.centrale.centrale) ? true : false;
};
/**
 * Return if the reference window is the dashboard planning page
 * @returns
 */
function isPlanningDashboardReferenceWindow(){
	try {
		return (rootPage.jqDialogIframe && rootPage.jqDialogIframe.userInterface && rootPage.jqDialogIframe.userInterface.centrale)? true : false;
	} catch(e) {
		return false;
	}
};
/**
 * Return if the reference window is the standard planning page
 * Author: Tantalo Christian Alessandro
 */
function isPlanningStandardReferenceWindow(){
	return rootPage.$("iframe#jqDialogIframe_jQPDataAction").contents().find("iframe#userInterface").contents().find("iframe#pCubeModels").size()>0;
};

/**
 * Return if the reference window is the report page
 * @returns
 */
function isReportReferenceWindow(){
	return (rootPage.centrale && rootPage.centrale.right) ? true : false;
};
/**
 * Return the reference window
 * @returns
 */
function getReferenceObjWindow(){
	if (isPlanningStandardReferenceWindow()){
		return getStandardPlanningInterfaceType();
	}else if(isPlanningDashboardReferenceWindow()){
		return rootPage.jqDialogIframe.userInterface.centrale;
	}else if (isDashboardReferenceWindow()){
		return rootPage.centrale.centrale;
	}else if (isReportReferenceWindow()){
		return rootPage.centrale.right;
	}else{
		throw new Error("getReferenceObjWindow: reference window is null."); 
	}
};
/**
 * Return the standard planning reference window
 * Author: Tantalo Christian Alessandro
 */
function getStandardPlanningInterfaceType(){
	var iframeJqDialog = rootPage.$("iframe#jqDialogIframe_jQPDataAction");
	if (iframeJqDialog.size()>0) {
		var iframePCubeModels = iframeJqDialog.contents().find("iframe#userInterface").contents().find("iframe#pCubeModels");
		if (iframePCubeModels.size()>0) {
			var iframePCubeModel = iframePCubeModels.contents().find("iframe#pCubeModel");
			var iframeDirectLink = iframePCubeModel.contents().find("iframe#directLinkToMask");
			var iframeRightPCubeModel = iframePCubeModel.contents().find("iframe[name='right']");
			var iframeRightDirectLink = iframeDirectLink.contents().find("iframe[name='right']");
			if  (iframePCubeModel.size()>0 && iframeDirectLink.size()>0){
				return iframeRightDirectLink.get(0).contentWindow;
			}else if (iframePCubeModel.size()>0 && iframeRightPCubeModel.size()>0){
				return iframeRightPCubeModel.get(0).contentWindow;
			}else{
				return iframeJqDialog.get(0).contentWindow;
			} 
		}		 
	}
	return rootPage;
};

/**
 * When the modal drill throught dialog is closed
 */
function onCloseModalDialogDT(idReport, idDialog){
	//Il try/catch è necessario nel caso di IE; nel caso in cui avveniva il refresh dell'opener,
	//non era più possibile chiudere la dialog => T#http://martin.decisyon.office/projects/dcy_betatest_3/ticket/110693
	try{
		var fwin = getDTOpener(idReport);
		
		if (fwin && fwin.$('#OBJ'+idReport).size()>0){
			fwin.reShowReport(idReport);
		}
		
		fwin.hideLoadingOnCurrWindow(null);
		if (fwin && fwin.parent)
			fwin.parent.hideLoadingOnCurrWindow(null);
	}catch(e){
		
	}
};
/**
 * Check if the JS error is by third parties (google translate plugin, skype call in plugin, ...)
 * @param errMessage
 */
function isThirdPartiesJSError(errMessage){
	if (errMessage == 'Script error.'){
		return true;
	}
	return false;
};
/**
 * Refresh the scenario opener page 
 */
function closeScenarioPage(winToRefresh){
	if (!winToRefresh)
		winToRefresh = getReferenceObjWindow();
	showWaitingForPlanning();
	var refObjWindow = winToRefresh;
	refObjWindow.location.reload(true);
	hideWaitingForPlanning();
};
/**
 * Returns if the page is opened in drill through
 */
function isPageOpenedInDrillThrough(){
	//This is current parent window
	if(parent === rootPage){
		return false;
	}
	var $noModalWin = rootPage.$('#noModalDialogFrame');
	return (($noModalWin && $noModalWin.length > 0 && $noModalWin.getAttribute('name').indexOf('winDT_')!=-1) || (rootPage.jqDialogIframe && rootPage.jqDialogIframe.frameDSH))? true : false;
};
/**
 * Returns  the window object opened in drill through
 */
function getObjWinOpenedInDrillThrough(){
	if (window.parent.parent.document.getElementById('noModalDialogFrame')){
		var noModalWin = window.parent.parent.document.getElementById('noModalDialogFrame');
		if (noModalWin && noModalWin.length > 0 && noModalWin.getAttribute('name').indexOf('winDT_')!=-1)
			return rootPage.noModalDialogFrame;
	}
	if (rootPage.jqDialogIframe && isOpenedOneDTDialog())
		return getOpenedOneDTDialog();
	
	return rootPage.centrale;
};
/**
 * Open a task 
 * @param imgNode
 */
function openTask(imgNode, scope, event){
	openTaskDialog(imgNode.getAttribute('params'), scope);
};
/**
 * Attach a task from context menu
 * @param attrParams
 */
function attachTask(attrParams){
	openTaskDialog(attrParams);
};
/**
 * Open the task into a new window
 * @param attrParams
 */
function openTaskDialog(attrParams, scope){
	logToConsole('********************************************************************************* open task');
	logToConsole(attrParams);
	logToConsole(unescape(attrParams));
	//logToConsole(decodeURIComponent(unescape(attrParams)));
	//var context =  decodeURIComponent(unescape(attrParams)).replace(/\+/g,' ');
	var context = unescape(decodeURIComponent(attrParams)).replace(/\+/g,' ');
	var contextData = context.substring(context.indexOf('rasterCtx=')+'rasterCtx='.length);
	var controllerUid = context.substring(context.indexOf('contrID=')+'contrID='.length, context.indexOf('rasterCtx=')-1);//contrID=PRVCTRL65348817024562&
		
	//#########################################################
	var ctxParse = JSON.parse(contextData);
	
	var refObjKeyDesc2 	= getCtxValue('refObjKeyDesc2',ctxParse.ctx) ;
	var refObjKeyDesc 	= getCtxValue('refObjKeyDesc',ctxParse.ctx) ;
	
	if( (isDefined(refObjKeyDesc2) && refObjKeyDesc2 !='') || (isDefined(refObjKeyDesc2) && refObjKeyDesc2 !='' )){
		var ctxMng = new ctxManager(ctxParse.ctx);
		
		ctxMng.set("refObjKeyDesc2", getTextEncode(refObjKeyDesc2));
		
		var objKeyDesc = JSON.parse(refObjKeyDesc);		
		if(isDefined(objKeyDesc) && isDefined(objKeyDesc.data) && (objKeyDesc.data instanceof Array))
		{
			for(var i=0; i<objKeyDesc.data.length; i++){
				objKeyDesc.data[i] = getTextEncode(objKeyDesc.data[i]);
			}
		
			objKeyDesc = JSON.stringify(objKeyDesc);
			ctxMng.set("refObjKeyDesc", objKeyDesc);
		}		
		
		contextData = ctxMng.toString();

	}
	
	//#########################################################
	rootPage.discCtrlId = controllerUid;
	
	var jqDialogTaskId = 'jqDialogTask';
	// Da implementare su jqDialog
//	createJQDialogByDPrWithOptions(jqDialogTaskId, controllerUid, contextData, "Task", "670", "520");
	
	var beforeCloseCallBack;
	
	var refObjType = getRefObjectTypeFromCtx(ctxParse.ctx);
	if (parseInt(refObjType) == 125){	// 125= PLANNING
		beforeCloseCallBack = "reloadWinOpener('"+jqDialogTaskId+"');refreshMashbordPostModify('"+jqDialogTaskId+"','"+scope+"')";
	}
	else{
		beforeCloseCallBack = "refreshMashbordPostModify('"+jqDialogTaskId+"','"+scope+"')";
	}
	
	createJQDialogByDPrWithCallbackClose(jqDialogTaskId, controllerUid, contextData, getLabel('taskDlgTitle'), "670", "520", beforeCloseCallBack);
};

/**
 * View the task image tooltip
 */
function viewTaskTooltip(idDivToView, event){
	var divTaskTooltip = document.getElementById(idDivToView);
	var imgTaskTooltip = getEventCell(event);
	var objClone = document.getElementById('dup_'+divTaskTooltip.id);
	if (objClone){			
		document.body.removeChild(objClone);
	}
	var objToAppend = divTaskTooltip.cloneNode(true);
	objToAppend.id = 'dup_'+divTaskTooltip.id;	
	objToAppend.style.display = '';	
	appendToBody(objToAppend, imgTaskTooltip, 24, 17, false, 0);		
};


/**
 * Hide the task image tooltip
 */ 
function hideTaskTooltip(idDivToView, event){
	var divTaskTooltip = document.getElementById('dup_'+idDivToView);
	divTaskTooltip.style.display = 'none';		
};

/**
 * Returns the list of the users connected on the viewed dashboard/report
 * @param mshPageId
 * @param type
 * @param owner
 */
function getConnectedUsersOn(node){
	var idDialog = "jqConnectedUsers";	
	var ctx = getCtx(node);
		
	var refObjId = getCtxValue('refObjId', ctx);
	var refObjType = getCtxValue('refObjectType', ctx);
	var owner = getCtxValue('owner', ctx);
	
	var ctxNew = buildNewCtxObject(); 
 	addDataToCtx(ctxNew, 'refObjId', refObjId); 
 	addDataToCtx(ctxNew, 'refObjectType', refObjType); 
 	addDataToCtx(ctxNew, 'owner', owner);
 	addDataToCtx(ctxNew, 'requireList', 'true');
 	addDataToCtx(ctxNew, 'runPrivateDiscussion', 'true');
 	var rasterCtx = getRasterCtxContainer(ctxNew); 
 	
 	rootPage.createJQDialogByDPrNoResizableNoModal(idDialog, 'usersOn', rasterCtx, getEncodedLanguageString('connectedUsers'), 700, 500);
};


/**
 * Mark the task
 * @param owner
 * @param taskId
 * @param markType
 * @param value
 * @param idObjToRefresh
 */
function markTask(owner, taskId, markType, value, idObjToRefresh){		
	try {
		var objToRefresh = document.getElementById(idObjToRefresh);
		var sendObjId = {
			url: "empty.jsp.cas",
			handleAs: "text",			
			content: {revent : 'MARK_TASK', owner : owner, taskId : taskId, markType: markType, value: value },
			contentType: "application/x-www-form-urlencoded; charset=utf-8",
			load: function(response,ioArgs){
				if(response.search("<HANDLE_ERROR>")!= -1){
					alerting('-'+response.replace("<HANDLE_ERROR>","")); 
				}else{
					objToRefresh.style.color = "red";
				}
			}
		};
		var requestObj = dojo.xhrPost(sendObjId);			
	}catch (e) {alert('exception '+e.name + e.message);}			
};
/**
 * Returns the key code
 * @returns {Boolean}
 */
function getEventKeyCode(event){
	return event.which || event.keyCode;
};

/**
 * Open a discussion 
 * @param imgNode
 */
function openDiscussion(imgNode){
	openDiscussionDialog(imgNode.getAttribute('params'));
};
/**
 * Attach a discussion from context menu
 * @param attrParams
 */
function attachDiscussion(attrParams){
	openDiscussionDialog(attrParams);
};

/**
 * Refresh the opener window (task, note)
 */
function refreshWindowOpener(jsDialogId){
	if (jsDialogId == undefined) {
		return;
	}
	
	var $jsDialog = rootPage.$('body').find("#"+jsDialogId);
	
	if ($jsDialog.size()==0) {
		return;
	}
	
	var winOpener = rootPage.getWindowReferenceFromRootPage(jsDialogId);
//	var ctx = $jsDialog.find('dcyContent').data('ctx');
	//var ctx = getCtx(rootPage.$('#jqDialogTask dcyContent'));
	var ctx = rootPage.getCtx($jsDialog.find('dcycontent'));
	
	var refObjType = getRefObjectTypeFromCtx(ctx);
	var refObjKey = getRefObjectKeyFromCtx(ctx);
	var refReportRequestId = getRefReportRequestIdFromCtx(ctx);
		
	if (jsDialogId == 'jqDialogTask'){
		$jsDialog.data('taskChangedStatus', true);
	}	
	
	try{
		var idDshPage = winOpener.document.dshIDReference;

		if (refReportRequestId!="" && 
				(isReportOpenerToRefresh(winOpener, refReportRequestId) 
				|| (idDshPage!=undefined && parseInt(refObjType) == 1))){
			winOpener.reShowReport(refReportRequestId);
			winOpener.focus();
		}		
		
		var owner = getOwnerModelForRefresh(winOpener.csMLO, idDshPage);
		if (isDshToRefresh(owner, idDshPage, refObjKey)) {
//			winOpener.location.href = "../dsh/dshPage.jsp.cas?revent=dStaRender&csMLO="+owner+"&dshID="+idDshPage;
			try {
				if (winOpener && isDefined(winOpener.isMashbordModified)) {
					winOpener.isMashbordModified = true;
				}
			} catch(e) {}
		
		}
	}catch(e){logToConsole('refreshWindowOpener: '+ e.message);}
};

function getOwnerModelForRefresh(owner, idDshPage) {
	if (idDshPage == '279367059626335') {
		owner = 'MASTERM';
	}
	else if (owner == 'MASTERM' || owner == 'SOCIALM') {
		owner = null;
	}
	return owner;
}

function isDshToRefresh(owner, idDshPage, refObjKey) {
	return isDefined(owner) && isDefined(idDshPage) && (idDshPage == '279367059626335' || refObjKey=="dshObjectTitle");
}

/**
 * Open the discussion into a new window
 * @param attrParams
 */
function openDiscussionDialog(attrParams){	
	var context = decodeURIComponent(unescape(attrParams)).replace(/\+/g,' ');
	var contextData = context.substring(context.indexOf('rasterCtx=')+'rasterCtx='.length);
	var controllerUid = context.substring(context.indexOf('contrID=')+'contrID='.length, context.indexOf('rasterCtx=')-1);//contrID=PRVCTRL65348817024562&
		
	var ctx = JSON.parse(contextData);
	var refObjContextDs = JSON.parse(getCtxValue("refObjContextDs", ctx.ctx));
	var title = refObjContextDs.title;
	var data = refObjContextDs.data;
	
	var desc = "";
	if (data != null) {
		desc = data[0];
		for (var i=1;i<data.length;i++)
			desc += " - " + data[i];
		desc = " > " + desc;
	}
		
	var dialogTitle = getLabel('discussion') + ": Report '" + title + "'" + desc;
	
	dialogTitle = getTextEncode(dialogTitle);
	var jqDialogDiscussionId = 'jqDialogDiscussion';
	
	rootPage.discCtrlId = controllerUid;
	
	var infiniteScroll = true;
	
	createJQDialogWithDataProvider(jqDialogDiscussionId,function(){refreshWindowOpener(jqDialogDiscussionId);},"", "", 
			"", "", dialogTitle, "800", "600", "", "", true, true, 
			false, "", false, false, "", "", controllerUid, contextData, null, false, infiniteScroll);
		
};


/**
 * Restituisce l'outer html di un nodo. La funzione è necessaria in quanto non tutti i browser
 * supportano l'attributo outerHTML. 
 */
function getOuterHTML(node) {
	if (node) {	
		if (node.outerHTML) {
			// Restituisco l'outerHTML se supportato
			return node.outerHTML;
		}
		else if(getNodeType(node) == 3) {
			//return node.wholeText;
			return node.data;
		}
		else {
			// Creo un nodo di appoggio
			var appNode = document.createElement('APP');
			// Clono il nodo interessato

			var clone = $(node).clone();

			// 	Inserisco il clone nel nodo di appoggio
			clone.appendTo(appNode);

			// Restituisco l'innerHTML del nodo di appoggio che corrisponde all'outeHTML del nodo
			return appNode.innerHTML;
		}
	}
	
	return "";
};

/**
 * Return the reference object id from ctx
 * @returns
 */
function getRefObjIdFromCtx(ctx){
	return getValueFromCTX('refObjId', ctx);
};
/**
 * Return the reference report request id from ctx
 * @returns
 */
function getRefReportRequestIdFromCtx(ctx){
	return getValueFromCTX('idReportRequest', ctx);
};
/**
 * Return the reference object type from ctx
 * @returns
 */
function getRefObjectTypeFromCtx(ctx){
	return getValueFromCTX('refObjectType', ctx);
};
/**
 * Return the  reference object key from ctx
 * @returns
 */
function getRefObjectKeyFromCtx(ctx){
	return getValueFromCTX('refObjectKey', ctx);
};

/**
 * Function to generate a random string value
 * @returns: random value
 */
function getRandomString() {
	return (Math.random()+' ').substring(2,10)+(Math.random()+' ').substring(2,10);
}

/**
 * Return the drill_through opener page.
 */
function getDTOpener(repID){
	var fwin = null;
	var isDTDialog = false;
	rootPage.$('iframe').each(function(index){
		var idDialog = $(this).attr('id');
		if ((idDialog.indexOf('jqDialogIframe')!=-1 && idDialog.indexOf(repID)!=-1) || (idDialog.indexOf('noModalDialogFrame')!=-1 && $(this).attr('name').indexOf(repID)!=-1)) {
			isDTDialog = true;
			if (idDialog.indexOf(repID)!=-1){
				var idD = idDialog.replace('jqDialogIframe_','');
				fwin = getWindowReferenceFromRootPage(idD);
			}else if ($(this).attr('name').indexOf(repID)!=-1){
				var idD = $(this).attr('name').replace('d_','');
				fwin = getWindowReferenceFromRootPage(idD);
			}
			if (fwin==null){
				fwin = (rootPage.centrale.centrale)? rootPage.centrale.centrale : rootPage.centrale.right;
			}
		}
	});		
	
	if (!isDTDialog){
		fwin = window.parent;
		while (fwin.parent.opener != null){
			fwin = fwin.parent.opener;
		}
	}
	
	return fwin;
};

function closeDshGroupTree() {
	var $btn = rootPage.$("iframe#centrale").contents().find("iframe#alto").contents().find("#btnChangeReportSize");
	var $box = rootPage.$("iframe#centrale").contents().find("iframe#alto").contents().find("#btnChangeReportSizeBox");
	var $img = rootPage.$("iframe#centrale").contents().find("iframe#alto").contents().find("#btnChangeReportSize-Img");
	if($box.hasClass($btn.attr("cssboxtoggled"))){
		hideFrameWithoutAnimation();
		$img.attr("src", $btn.attr("imgurl"));
		$box.removeClass($btn.attr("cssboxtoggled"));
		$box.addClass($btn.attr("cssnormal"));
	}

}
function checkUrlEncode(utf8CheckString) {
    $.ajax({
        type: "GET",
        data: {revent:'LW_CHECK_URL_ENCODE', a0:utf8CheckString},
        contentType: 'text/html'
    });        
};

//allow to include in the document another javascript dynamically
function includeJS(script_filename) {
    document.write('<script');
    document.write(' language="javascript"');
    document.write(' type="text/javascript"');
    document.write(' src="' + script_filename + '">');
    document.write('</script>');
}

//allow to include in the document another javascript dynamically
function includeCSS(script_filename) {
    document.write('<link');
    document.write(' rel="stylesheet"');
    document.write(' type="text/css"');
    document.write(' href="' + script_filename + '"/>');
}

/**
 * Funzione che clona un oggotto javascript
 */
function cloneObject(obj) {
	if (obj)
		return JSON.parse(JSON.stringify(obj));
	return [];
}

/**
 * Return if the opener report must be refreshed
 * 
 * @param winPopup
 * @param refReportRequestId
 * @returns {Boolean}
 */
function isReportOpenerToRefresh(winPopup, refReportRequestId){
	var idRepOpener = winPopup.idRepReq;
	if (idRepOpener!=undefined && idRepOpener==refReportRequestId) {
		return true;
	}
	return false;
};

/**
 * Return the value from json
 * @param id
 * @param ctx
 */
function getValueFromCTX(id, ctx){
	if (ctx){
		for (var i=0; i<ctx.length; i++) {
			if (ctx[i].id == id) {
				return ctx[i].value; 
			}
		}
	}
	return "";
};

function killUserSessionOnUnload() {
	
	$.ajax({
		async:false,
		data: {revent:'LW_WINDOW_CLOSED',noShowError:'1'},
		success: function(data, textStatus, xhr) {
		}
	});
	
}

function getJqDialogJSCode(dataProviderId, ctx, title, width, height) {
	  var fun = "createJQDialogByDPrWithOptions(";
	  fun += "$(this).attr('id'), ";
	  fun += getParamFunctionByType(dataProviderId) + ", ";
	  fun += "'" + ctx.replace(/\"/g,"\\'") + "', ";
	  fun += getParamFunctionByType(title) + ", ";
	  fun += getParamFunctionByType(width) + ", ";
	  fun += getParamFunctionByType(height);
	  fun += ");";
	  return fun;
}

/**
 * Open a dashboard page from welcome section
 * @param owner
 * @param contentId
 */
function openMashboardFromWelcome(owner, contentId, groupDshID){
	rootPage.navigateToContent('../dsh/dshMain.jsp.cas?revent=LOAD_DSH_TREE&setMainModel=1&csMLO='+owner+'&dshID='+contentId+'&groupDshID='+groupDshID);
};

/**
 * Implements a function for let an object to be draggable, on touch devices, using hammer library
 * Author: Tantalo Christian Alessandro
 */

$.fn.hammerDraggable = function() {
    return this.each(function() {
        var el = $(this);

		$(el).hammer()
	    .on("touch", function(e) {
	    	el.data("posX", el.offset().left);
	    	el.data("posY", el.offset().top);
	    })
	    .on("drag", function(e) {
	    	var posX = parseInt(el.data("posX"));
	    	var posY = parseInt(el.data("posY"));
	    	var deltaX = parseInt(e.gesture["deltaX"]);
	    	var deltaY = parseInt(e.gesture["deltaY"]);

	    	el.offset({left: posX+deltaX, top:posY+deltaY});
	    	e.gesture.preventDefault();
	    });
    });
};

/**
 * Replace the element with nodes parsed from the given string. 
 * If the browser supports the outerHTML property, it uses it.
 * @param node
 * @param newHtml
 */
function setOuterHTML(node, newHtml){
	if (node) {	
		$(node).empty();
		if (node.outerHTML) {
			if (ENV_VARS.isOnIe8() && node.getAttribute("dshtype")==146) {
				var newElement = $(newHtml).get(0);
				
				node.parentNode.replaceChild(newElement, node);				
			}
			else {
				node.outerHTML = newHtml;
			}						
		}else{
			
			var parentNode = node.parentNode;
			var e = document.createElement("div");
		    var newNode = null;
		    e.innerHTML = newHtml;
		    for(var i=0; i<e.childNodes.length; i++) {
		        if(e.childNodes[i].nodeType == 1) {
		        	newNode = e.childNodes[i];
		            break;
		        }
		    }
		    if(newNode){
		    	parentNode.insertBefore(newNode, node);
		    	parentNode.removeChild(node);
		    }   
		}
	}
	
    
};
/**
 * Delay the callback function according the milliseconds
 */
var beLate = (function(){
    var timer = 0;
    return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
})();

function getOptionsForTooltip(){
	return optionsForTooltip = {
			style: {
				classes: 'de_qtip qtip-listMenu'
			}
	};
}
/**
 * Open the menu for the contents report row
 * @param obj
 * @param refObjId
 * @param refObjType
 * @param owner
 */
function openDshContentsMenu(obj, refObjId, refObjType){
	var ctxNew = buildNewCtxObject(); 
 	addDataToCtx(ctxNew, 'refObjId', refObjId); 
 	addDataToCtx(ctxNew, 'refObjectType', refObjType); 
 	var rasterCtx = getRasterCtxContainer(ctxNew); 
	createJQTooltipByDataProvider($(obj), "dshContentsMenu", rasterCtx, getOptionsForTooltip());
};
/**
 * Open the menu for the spaces report row
 * @param obj
 * @param refObjId
 * @param refObjType
 * @param owner
 */
function openDshSpacesMenu(obj, refObjId, refObjType){
	var ctxNew = buildNewCtxObject(); 
 	addDataToCtx(ctxNew, 'refObjId', refObjId); 
 	addDataToCtx(ctxNew, 'refObjectType', refObjType); 
 	var rasterCtx = getRasterCtxContainer(ctxNew);
	createJQTooltipByDataProvider($("a", $(obj)), "dshSpacesMenu", rasterCtx, getOptionsForTooltip());
};
/**
 * Open the menu for the subjects report row
 * @param obj
 * @param refObjId
 * @param refObjType
 * @param owner
 */
function openDshSubjectsMenu(obj, refObjId, refObjType){
	var ctxNew = buildNewCtxObject(); 
 	addDataToCtx(ctxNew, 'refObjId', refObjId); 
 	addDataToCtx(ctxNew, 'refObjectType', refObjType); 
 	var rasterCtx = getRasterCtxContainer(ctxNew); 
	createJQTooltipByDataProvider($(obj), "dshSubjectsMenu", rasterCtx, getOptionsForTooltip());
};
/**
 * Called on prefferd button click on the contents reports(spaces, subjects, contents)
 */
function setPreferenceLevel(obj, refObjId, refObjectType, owner){
	var $obj = (obj.tagName == 'TD' || obj.tagName == 'LI') ? $(obj).find('a') : $(obj);
	var classNotFavourite = 'de_notfavourite_placeholder';
	var classFavourite = 'de_favourite_placeholder';
	var classSuperFavourite = 'de_superfavourite_placeholder';
	var currentClass = '';
	
	if ($obj.hasClass(classNotFavourite)){
		currentClass = classNotFavourite;
		$obj.removeClass(classNotFavourite);
		$obj.addClass(classFavourite);
	}else if ($obj.hasClass(classFavourite)){
		currentClass = classFavourite;
		$obj.removeClass(classFavourite);
		$obj.addClass(classSuperFavourite);
	}else{
		currentClass = classSuperFavourite;
		$obj.removeClass(classSuperFavourite);
		$obj.addClass(classNotFavourite);
	}
	/*
	if (refObjId!=null)
		setCtxNodeValue(obj, 'refObjId', refObjId);
	if (refObjectType!=null)
		setCtxNodeValue(obj,'refObjectType', refObjectType);
	if (owner!=null)
		setCtxNodeValue(obj, 'owner', owner);
	var ctx = getRasterCtxContainer(getCtx(obj));
	*/
	var ctxNew = buildNewCtxObject(); 
 	addDataToCtx(ctxNew, 'refObjId', refObjId); 
 	addDataToCtx(ctxNew, 'refObjectType', refObjectType); 
 	addDataToCtx(ctxNew, 'owner', owner); 
 	var ctx = getRasterCtxContainer(ctxNew); 
 	
	$.ajax({
		  data: {revent:'SOCIAL_PREFERRED', a0:ctx},
		  error: function(data, textStatus, xhr)  {
				$obj.removeClass();
				$obj.addClass(currentClass);
		  } 
	  });
};
/**
 * Call the dashboard to update the area
 * @param obj
 */
function editSelectedSpace(obj, spaceId, spaceType){
	var paramsToAdd = '&revent=LW_DSH_LINK&EDITING_AREA_ID='+spaceId;
	showSpaceWizard(spaceType, paramsToAdd);
};

/**
 * Remove image resource
 * @param contextId
 * @param contextType
 * @param contextOwner: optional
 * @param callbackClose: optional
 */
function removeImageResource(contextId, contextType, contextOwner, callbackClose) {
	$.ajax({
		data: {revent:'LW_REMOVE_IMAGE_RESOURCE',contextId:contextId, contextType:contextType, contextOwner:contextOwner},
		success: function(data, textStatus, xhr) {
			if (callbackClose)
				eval(callbackClose+"('"+contextType+"', '"+contextId+"', '"+contextOwner+"')");
		}
	});
}

/**
 * Effettua il refresh dell'immagine del profilo sul top
 */
function refreshUserTopImage(idUser) {
	try{
		var $topImages = rootPage.$('img[usrid|="'+idUser+'"]');
		$topImages.each(function(i, value) {
			$(value).attr('src', getImageTokenLink(idUser, "NA", "PROFILE_IMAGE", "S32"));
		});
	} catch (e) {
		alert(e);
	}
}

/**
 * Costruisce il link tokenizzato ad una immagine per cui è stato fatto l'upload
 */
function getImageTokenLink(ctxId, csMLO, ctxType, ctxSize, ctxSubType) {
	var subTypeString = "";
	if (ctxSubType != null)
		subTypeString = "&ctxSubType="+ctxSubType;
	var result = "../.." + ENV_VARS.getValue('imageResourceToken') + "/img.png?ctxId="+ctxId+"&ctxType="+ctxType+"~"+ctxSize+"&csMLO="+csMLO+subTypeString;
	result = result.replace("/T00T/","/T"+getRandomString()+"T/");
	return result;
}

/**
 * Open the space wizard mashboard
 * @param spaceType
 */
function showSpaceWizard(spaceType, paramsToAdd){
	var owner = 'MASTERM';
	var mshSpaceWizardId = "";
	paramsToAdd = (paramsToAdd!=undefined) ? paramsToAdd : "";
	var params = (paramsToAdd.indexOf('revent')!= -1) ? paramsToAdd+"&IN_EDIT=1" : paramsToAdd +'&revent=dStaRender&IN_EDIT=0';
	
	switch(spaceType) {
	  	case "1005": // AREA
	  		mshSpaceWizardId = '653351328166400';
	  		break;
	  	 
	  	case "1010": // GROUP
	  		mshSpaceWizardId = '235358326504414';
	  		break;
	  	
	  	case "1011": // INITIATIVE
	  		mshSpaceWizardId = '76363451682779';
	  		break;
	  	default:
			
	}
	var mshPageToCall_Url = '../dsh/dshPage.jsp.cas?dshID=' + mshSpaceWizardId + '&csMLO=' + owner + params;
	if (rootPage && rootPage.centrale){
		rootPage.centrale.location.href = rootPage.centrale.centrale = mshPageToCall_Url;
	 }
};

/**
 * Mostra il tooltip della sintesi dell'utente
 * @param currentNode
 * @param userId
 */
function showUserTip(currentNode, userId, scope, followCallback) {

	var idDialogContainer = getIdDialogContainer(currentNode);
	
	var ctxObject = buildNewCtxObject();
	
	if(!isDefined(scope) || scope == true){
		if(isDefined(idDialogContainer)){
			scope="ONDIALOG";
		}else{
			scope="DEFAULT";			
		}
	}
	
	addDataToCtx(ctxObject, 'userId', userId);
	addDataToCtx(ctxObject, 'scope', scope);
	if (!isDefined(followCallback)) {
		addDataToCtx(ctxObject, 'followCallback', followCallback);
	}
	
	var userTipOptions = getOptionsForCloseTooltip(false, 400);

	createJQTooltipWithDataProvider($(currentNode), LANGUAGES.getOriginalLabel("userInfo"), 'userInfo', userTipOptions, getRasterCtxContainer(ctxObject), 320, 'userInfo');
};

function showSpaceTip(currentNode, spaceId) {
	//id riservati non vengono mostrati: es everyone
	if (spaceId>=0) {
		var opt = getOptionsForCloseTooltip(false, false, 'qtip-socialAreasInfo');
		opt.hide.inactive = false;
		var ctxObject = buildNewCtxObject();
		addDataToCtx(ctxObject, 'spaceId', spaceId);
		
		createJQTooltipWithDataProvider($(currentNode),  LANGUAGES.getOriginalLabel("spaceInfo"), "socialAreasInfo", opt, getRasterCtxContainer(ctxObject), 320, 'spaceBoxInfo');
	}
}

function stopPropagation(event) {
	if(!event)
		return;
	
	if ($.browser.msie)
		event.cancelBubble = true;
	else
		event.stopPropagation();
}

/**
 * Call the confirm dialog to delete the space
 * @param obj
 */
function deleteSelectedSpace(obj, spaceId, spaceType, event){
	JQConfirm("deleteSocialSpace", LANGUAGES.getOriginalLabel("DeleteSocContTitle") , LANGUAGES.getOriginalLabel("confirmToRemove_"+spaceType),function(){rootPage.centrale.removeSelectedSpace(obj,spaceId,spaceType,event);});
};
/**
 * Delete the space
 * @param obj
 * @param spaceId
 * @param spaceType
 */
function removeSelectedSpace(obj, spaceId, spaceType, event){
	callSocialSpaceHandler(spaceId, "", spaceType, "", "DELETE_SPACE", 'SHOW_LIST', obj, null, null, event);
};
/**
 * Open a space from welcome section
 * @param owner
 * @param contentId
 */
function openSpaceFromWelcome(owner, contentId, type){
	if (type==1){
		return manageContentForwardSpace('SOCIALM', contentId, '1005', 'SOCIAL_AREA_NORMAL');
	}else if (type==2){
		return manageContentForwardSpace('SOCIALM', contentId, '1010', 'SOCIAL_AREA_OF_GROUP');
	}else if (type==3){
		return manageContentForwardSpace('SOCIALM', contentId, '1011', 'SOCIAL_INITIATIVE');
	}
	return "";
};

/**
 * Close the space
 * @param obj
 * @param spaceId
 * @param spaceType
 */
function closeSelectedSpace(obj, spaceId, spaceType, event){
	callSocialSpaceHandler(spaceId, "", spaceType, "", "CLOSE_SPACE", 'SHOW_LIST', obj, null, null, event);
};
/**
 * Call the confirm dialog to close the space
 * @param obj
 */
function confirmCloseSelectedSpace(obj, spaceId, spaceType, event){
	JQConfirm("closeSocialSpace", LANGUAGES.getOriginalLabel("Close") + " " + LANGUAGES.getOriginalLabel("space"), LANGUAGES.getOriginalLabel("confirmToClose_"+spaceType),function(){rootPage.centrale.closeSelectedSpace(obj,spaceId,spaceType,event);});
};

/**
 * Reopen the space
 * @param obj
 * @param spaceId
 * @param spaceType
 */
function reopenSelectedSpace(obj, spaceId, spaceType, descContentType, event){
	callSocialSpaceHandler(spaceId, "SOCIALM", spaceType, descContentType, "REOPEN_SPACE", 'FORWARD_SPACE', obj, null, null, event);
};
/**
 * Call the confirm dialog to reopen the space
 * @param obj
 */
function confirmReopenSelectedSpace(obj, spaceId, spaceType, descContentType, event){
	JQConfirm("reopenSocialSpace", LANGUAGES.getOriginalLabel("Reopen") + " " + LANGUAGES.getOriginalLabel("space"), LANGUAGES.getOriginalLabel("confirmToReopen_"+spaceType),function(){rootPage.centrale.reopenSelectedSpace(obj,spaceId,spaceType,descContentType,event);});
};

/**
 * 	Create a in-memory div, set it's inner text(which jQuery automatically encodes)
  	then grab the encoded contents back out.  The div never exists on the page.
  	Basically a div element is created in memory, but it is never appended to the document.
	On the htmlEncode function I set the innerText of the element, and retrieve the encoded innerHTML.
 */
function getHTMLEncode(value){
	return $('<div/>').text(value).html();
};

/**
 * 	Create a in-memory div, set it's inner html(which jQuery automatically encodes)
  	then grab the encoded contents back out.  The div never exists on the page.
  	Basically a div element is created in memory, but it is never appended to the document.
	On the htmlEncode function I set the innerHTML of the element, and retrieve the encoded innerTEXT.
 */
function getTextEncode(value){
	return $('<div/>').html(value).text();
};

/**
 * Returns the color in hexadecimal format by passing the color in rgb format.
 * @color
 * */
function rgbToHex(color) {
    if (color.substr(0, 1) === "#") {
        return color;
    }
    var nums = /(.*?)rgb\((\d+),\s*(\d+),\s*(\d+)\)/i.exec(color),
        r = parseInt(nums[2], 10).toString(16),
        g = parseInt(nums[3], 10).toString(16),
        b = parseInt(nums[4], 10).toString(16);
    return "#"+ (
        (r.length == 1 ? r +"0" : r) +
        (g.length == 1 ? g +"0" : g) +
        (b.length == 1 ? b +"0" : b)
    );
}

function openContentShareDlg(refObjId, owner, refObjectType) {
	var ctx = new ctxManager();
	ctx.set("refObjId", refObjId);
	ctx.set("owner", owner);
	ctx.set("refObjectType", refObjectType);
	createJQDialogByDPrWithOptions('idContentShareDlg', 'contentShare', ctx.toString(), getLabel('contentShareTitle'), '600', '400', false);
}

function initContentShareTemplate() {
	$(document).ready(function(){
		$(".autogrowSharedLink").autoGrow();
      	$(".autogrowSharedLink").on("focus", function(e){
          	var $this = $(this);
         	$this.select();
          	$this.mouseup(function() {
				$this.off('mouseup');
				return false;
				});
      	});
	});
}

function loadingButton($button) {
	if (isDefined($button) && $button.size()) {
		var $span = $("<span class='button_loading'></span>");		
		$button.find('span').css('display','none');
		$button.attr("disabled", "disabled");
		$button.css("background-color", "transparent");
		$button.css("border", "none");
		$button.append($span);
	}
}

function unloadingButton($button) {
	if (isDefined($button) && $button.size()) {
		$button.find('span').css('display','inline');
		$button.removeAttr("disabled");
		$button.find(".button_loading").remove();
		$button.css("background-color", "");
		$button.css("border", "");
	}
}

$.fn.isDisabledButton = function() {
	return this.is('[disabled=disabled]');
};

/**
 * Ritorna l'id dell'oggetto del mashbord a partire dal custom-js-id impostato
 * @param objectJsID
 * @returns
 */
function toObjectId(objectJsID) {
	return $('[customId="'+objectJsID+'"]').attr("objId");
}

/**
 * Ritorna reportViewId a partire dal custom-js-id dell'oggetto crosstab
 * @param objectJsID
 * @returns
 */
function toReportViewId(objectJsID) {
	return $('[customId="'+objectJsID+'"]').attr("reportViewId");
}


/**
 * Identifica se sull'elemento è presente/predisposta una scrollBar
 */
(function($) {   
    function hasScroll(el, axis) {
        var $el = $(el);
        var sX, sY;
        var h, w;
        var maxH, maxW;
        try {
        	sX = $el.css('overflow-x');
        	sY = $el.css('overflow-y');
        	h = $el.get(0).style.height;
			w = $el.get(0).style.width;
			maxH = $el.css('max-height');
			maxW = $el.css('max-width');
		} catch (e) {}
        
        var hidden  = 'hidden';
        var visible = 'visible';
        var scroll  = 'scroll';
        var auto    = 'auto';

        if (!axis || (axis != 'x' && axis != 'y')) { 
            if (sX === sY && (sY === hidden || sY === visible)) {
                return false;
            }
            if (sX === scroll || sY === scroll) { return true; }
            
            return $el.innerHeight() < el.scrollHeight-1 || $el.innerWidth() < el.scrollWidth;
        } else if (axis === 'x') {
            if (sX === hidden || sX === visible) { return false; }
            if (sX === scroll) { return true; }
            if (sX === auto && (maxW!='none') || (isDefined(w) && w.indexOf("px") != -1)) { return true; }
            
            return $el.innerWidth() < el.scrollWidth;
        } else  if (axis === 'y'){
            if (sY === hidden || sY === visible) { return false; }
            if (sY === scroll) { return true };
            if (sY === auto && (maxH!='none')|| (isDefined(h) && (h.indexOf("px") != -1))) { return true };
            
            return el.scrollHeight-$el.innerHeight() > 1
        }

        // non può essere deciso cosa si stia cercando, non si hanno i parametri necessari
        return $el.innerHeight() < el.scrollHeight
        || $el.innerWidth() < el.scrollWidth;
    }
    $.expr[':'].hasScroll = hasScroll;
    $.fn.hasScroll = function(axis) {
        var el = this[0];
        if (!el) { return false; }
        return hasScroll(el, axis);
    };
}(jQuery));


/**
 * Author: Pagano Francesco
 * 
 * Funzione per l'iliminazione degli attributi applicati ad un nodo in errore 
 * Rimuove la classe passata e l'eventuale qTip2 applicato
 * @param node
 * @param classe String
 *
 */
function removeErrorNodeTo(node, classe){
	$(node).removeClass(classe);
	$(node).qtip("destroy");
	$(node).removeAttr('state');
}

/**
 * Author: Pagano Francesco
 * 
 * Funzione per il settaggio della classe 'invalid' su un element/node passato 
 * Per segnalare il nodo in errore
 * @param node
 * @param classe String
 *
 */
function addInvalidClass($element){
	$element.addClass("invalid");
}

/**
 * Set the element target in error
 * @param targetSelector
 */
function setElementInError($targetSelector, $targetForTooltip, showTooltip, errMsg){
	addInvalidClass($targetSelector);

	if (!isDefined(errMsg)) {
		errMsg =  $targetSelector.attr('invalidMessage');
	}
	
	if (!isDefined(errMsg)) {
		errMsg = getLabel('unableToPerform');
	}
	if (showTooltip) {
		setTimeout(function() {
			createJQTooltipWithSimpleHTML($targetForTooltip, LANGUAGES.getOriginalLabel('Warning'), errMsg);
		}, 500);
	}
};

function manageErrorNode(role, node, label){
	var $node = $(node);
	var $tagErrorNode  = $node.find('#textContentTag');
	var textOfTagNode = $.trim($tagErrorNode.text());
	var textForTip 	  = textOfTagNode +"-"+ LANGUAGES.getOriginalLabel(label);
	$tagErrorNode.attr('onclick','setJQtipOnElement(this,"'+textForTip+'");stopPropagation(event);return false;').addClass('de_cursorHand');
	
	if (role == "edit") {
		var $invalidNode = $node.closest(".rootEditor");
	}
	else {
		var $invalidNode = $node.closest(".rootMeta");
	}
	
	addInvalidClass($invalidNode);
}

function getTipOptionForElementInError(){
	var myOptions = {				
			show : {
				solo: true
			},
			hide: {
				inactive: 3500,
				fixed: true 
			}
		};
	
	return myOptions;
}
/**
 * Author: Pagano Francesco
 * 
 * Funzione per il settaggio del primo nodo in errore con qTip anche applicato in una Dialog 
 * @param node
 * @param classe String
 * @param showJQTooltip : Mostrare o meno il toolTip
 */
function setFirstElementinError(showJQTooltip){
	
	!isDefined(showJQTooltip) ? showJQTooltip = true : null;
	
	var $firstElementinError;
	
	$firstElementinError 	=	$($.find(".invalid:first"));
			
	if ( ($firstElementinError.length) && showJQTooltip ){
		$firstElementinError.qtip("destroy");
		var errMsg = $firstElementinError.attr('invalidmessage');
			
		if (!errMsg) {
			errMsg = getLabel('unableToPerform');
		}
		
		//Timeout per gestire lo scroll verso l'elemento in errore
		setTimeout(function() {
			createJQTooltipWithSimpleHTML($firstElementinError, LANGUAGES.getOriginalLabel('Warning'), errMsg, getTipOptionForElementInError());
		}, 1000);
	}
	
	return $firstElementinError;
}
/**
 * Check if a drill-through dialog is opened
 */
function isOpenedOneDTDialog(){
	rootPage.$('[name="jqDialogIframe"]').each(function(index) {
		if ($(this).contents().find('[name^="frameDSH"]').size() > 0 ){
			return true;
		}
	});
	return false;
};
/**
 * Get a opend drill-through modal dialog
 */
function getOpenedOneDTDialog(){
	rootPage.$('[name="jqDialogIframe"]').each(function(index) {
		if ($(this).contents().find('[name^="frameDSH"]').size() > 0 ){
			return $(this).contents().find('[name^="frameDSH"]').eq(0)[0];
		}
	});
	return rootPage.centrale;
};

/**
 * Disable enterkey on form (for IE)
 * to use it in input: onKeyPress="return disableEnterKey(event)"
 * @param e
 * @returns {Boolean}
 */
function disableEnterKey(e) {
     var key;
     if (window.event) {
    	 key = window.event.keyCode;     //IE
     } else {
    	 key = e.which;     //firefox
     }

     if (key == 13) {
          return false;
     } else{
          return true;
     }
}


/**
 * Allow to use indexOf when there is still no native support.
 * ex. IE8
 */
if (!Array.prototype.indexOf) {
	  Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
	    'use strict';
	    if (this == null) {
	      throw new TypeError();
	    }
	    var n, k, t = Object(this),
	        len = t.length >>> 0;

	    if (len === 0) {
	      return -1;
	    }
	    n = 0;
	    if (arguments.length > 1) {
	      n = Number(arguments[1]);
	      if (n != n) { // shortcut for verifying if it's NaN
	        n = 0;
	      } else if (n != 0 && n != Infinity && n != -Infinity) {
	        n = (n > 0 || -1) * Math.floor(Math.abs(n));
	      }
	    }
	    if (n >= len) {
	      return -1;
	    }
	    for (k = n >= 0 ? n : Math.max(len - Math.abs(n), 0); k < len; k++) {
	      if (k in t && t[k] === searchElement) {
	        return k;
	      }
	    }
	    return -1;
	  };
}		

/*
 * Function per apertura URL in una new Window 
 * Author : Pagano Francesco
 */ 
function openLinkWindow(url, name, option){
	if(isDefined(url)){
		window.open(url , name, option);
	}
}

/*
 * Returns the event target (cross browser)  
 */
function getEventTarget(evt){
	// get window.event if argument is falsy (in IE)
	evt = evt || window.event; 
	// get srcElement if target is falsy (IE)
	var targetElement = evt.target || evt.srcElement;
	return targetElement;
}

/**
 * Metodo per il controllo che il contenuto di un elemento DOM sia valido(ex.title field on task edit)
 * Author : Pagano Francesco
 * @param $element : elemento DOM JQUERY
 * @param maxlenght : lunghezza max caratteri
 * @param errorMessage : messaggio di errore
 * @param showTipErr : se mostrare o meno il messaggio di errore
 * @returns {Boolean}
 */
function isElementContentValid($element, maxlenght, errorMessage, showTipErr){
	var elementVal	=	$element.val().trim();
	var result		= 	false;
	
	if(isDefined(maxlenght)){
		result		=	elementVal.length <= maxlenght && elementVal.length > 0;
	}
	else{
		result		= elementVal.length > 0;
	}
	
	if(result){
		removeErrorNodeTo($element,'invalid');
		$element.removeAttr("invalidMessage");
		return true;
	}
	else{
		!isDefined(errorMessage) ? errorMessage = LANGUAGES.getLabelHTMLEncod("unableToPerform") : null;
		
		addInvalidClass($element);
		$element.attr('invalidMessage', errorMessage);
		
		if(elementVal == ""){
			$element.val('');
		}
		
		if(showTipErr){
			createJQTooltipWithSimpleHTML($element, LANGUAGES.getLabelHTMLEncode("SocContValidationErrorTitle"), errorMessage);
		}
		
		return false;
	}
}

/**
 * This Method use for controll if dom element is visible
 * Author : Pagano Francesco
 * @param container : container 		<- the javascript element not jquery
 * @param element 	: element inside	<- the javascript element not jquery
 * @returns {Boolean}
 */
function isElementVisibleInContainer(container, element)
{
	var containerFind = null;
	
	if(!isDefined(container)){
		containerFind = $(findScrollbar($(selector).get(0).parentNode)).get(0);		
	}
	else{
		containerFind = container;
	}
	
	z = containerFind.getBoundingClientRect(); //Specifies the bounding rectangle of the current element
	r = element.getBoundingClientRect();//Specifies the bounding rectangle of the current element

	return   !(r.top>z.bottom || r.bottom<z.top || r.left>z.right || r.right<z.left); 
}

/**
 * This Method return elements find with scroll;
 * In this moment the scroll supported are : native scroll and slimm scroll
 * Author : Pagano Francesco
 * @param saveObj 	: array
 * @param element 	: node	<- the target node
 * @returns {array}
 */
//Return array that contains all scrollable nodes finded
function getElementsWithScroll(saveObj, node) {
	
	var hasScrollbar= $(node).hasScroll('y');
	var hasScrollbarClass = $(node).hasClass('slimScrollDiv');
	
	if (hasScrollbar) {
		saveObj.push(node);	
	}
	else if(hasScrollbarClass){
		saveObj.push($(node.children).get(0));
	}
		
	if (isDefined(node.parentNode)) {
		return getElementsWithScroll(saveObj, node.parentNode);
	}
	else{
		return saveObj;
	}
}

/**
 * Author: Pagano Francesco
 * This plugin is for manage multilines ellipsis
 */
/**
 * If content of element is numeric
 * @param $element
 * @param errorMessage
 * @returns {Boolean}
 */
function isNumberValidate($element, errorMessage){
	if($element.val() != "" && !$.isNumeric($element.val())) {
		addInvalidClass($element);
		$element.attr('invalidMessage', errorMessage);
		return false;
	}
	return true;
}

/**
 * Verify if element is numeric and positive
 * @author fferraiu
 * @param $element
 * @param errorMessage
 * @returns {Boolean}
 */
function isNumericAndPositiveValidate($element, errorMessage){
	if(!isNumberValidate($element, errorMessage)) {
		return false;
	}
	
	if ($element.val() <= 0) {
		addInvalidClass($element);
		$element.attr('invalidMessage', errorMessage);
		return false;
	}
	
	return true;
}

/**
 * Da un oggetto json estrae i parametri per passarli in url con i separatori previsti: 
 * #####: separatori valori multipli, 
 * @param params
 */
function buildUrlParams(params){
	var multiValuesSeparator = "#####";
	var paramsList = params;
	var _params="";
	for (var i in paramsList) {
		var isValuesList = (paramsList[i].value instanceof Array) ? true : false;
		if (isValuesList){
			var valuesList = paramsList[i].value;
			_params = _params + "&" + paramsList[i].name + "=";
			for (var j=0; j < valuesList.length; j++){
				_params = _params + encodeURIComponent(valuesList[j]) + multiValuesSeparator;
			}
	    }else{
	    	_params = _params + "&" + paramsList[i].name + "="+ encodeURIComponent(paramsList[i].value);
	    }
		_params = _params.substring(_params.length - 5) == multiValuesSeparator ? _params.substring(0, _params.length - 5) : _params;
	}
	return _params;
};
/**
 * Da un oggetto json estrae i parametri per passarli in url con gli attributi previsti per l'evento LW_REPORT_FORM_PARAMS: 
 * #####: separatori valori multipli, 
 * @param params
 */
function buildUrlParamsToRedirect(params){
	var paramsSeparator = "#_#_#";
	var multiValuesSeparator = "#####";
	var paramsList = params;
	var _paramsNames = "";
	var _paramsValues = "";
	var _valuesList = "";
	for (var i in paramsList) {
		_paramsNames = _paramsNames + paramsList[i].name + paramsSeparator;
		var isValuesList = (paramsList[i].value instanceof Array) ? true : false;
		if (isValuesList){
			var valuesList = paramsList[i].value;
			for (var j=0; j < valuesList.length; j++){
				_valuesList = _valuesList + "%27" + encodeURIComponent(valuesList[j]).replace(/'/g, "%27") + "%27" + multiValuesSeparator;
			}
			_valuesList = _valuesList.substring(_valuesList.length - 5) == multiValuesSeparator ? _valuesList.substring(0, _valuesList.length - 5) : _valuesList;
			_paramsValues = _paramsValues + _valuesList + paramsSeparator;
	    }else{
	    	var valuesList = paramsList[i].value;
	    	_paramsValues = _paramsValues + encodeURIComponent(valuesList).replace(/'/g, "%27") + paramsSeparator;
	    }
	}
	
	var _paramsNamesEnd = _paramsNames.substring(_paramsNames.length - 5) == paramsSeparator ? _paramsNames.substring(0, _paramsNames.length - 5) : _paramsNames;
	var _paramsValuesEnd = _paramsValues.substring(_paramsValues.length - 5) == paramsSeparator ? _paramsValues.substring(0, _paramsValues.length - 5) : _paramsValues;
	_paramsValuesEnd = _paramsValuesEnd.substring(_paramsValuesEnd.length - 5) == multiValuesSeparator ? _paramsValuesEnd.substring(0, _paramsValuesEnd.length - 5) : _paramsValuesEnd;
	
	var _params ="&paramName="+encodeURIComponent(_paramsNamesEnd)+"&paramValue="+encodeURIComponent(_paramsValuesEnd);
	//logToConsole(_params);
	return _params;
};
/**
 * Ritorna il tipo dell'oggetto nel mashbord a partire dal custom-js-id impostato
 * @param objectJsID
 * @returns
 */
function getDshTypeByCustomJsId(objectJsID) {
	return $('[customId="'+objectJsID+'"]').attr("dshType");
};


/**
 * Return the date in this format: mm/dd/yyyy h:m:s:ms
 * @returns {String}
 */
function getLocalDate(){
	var dNow = new Date();
	return (dNow.getMonth()+1) + '/' + dNow.getDate() + '/' + dNow.getFullYear() + ' ' + dNow.getHours() + ':' + dNow.getMinutes() + ':' + dNow.getSeconds()+':'+dNow.getMilliseconds();
};


(function($) {
	//private
	var mlinesEllipsisID = 0; //Id for secure option on multiple elements
	
    $.fn.mlinesEllipsis = function(_maxTextNodeManage)
    {	
		var $window = $(window),			
			MAX_TEXT_NODE_MANAGE = _maxTextNodeManage ? _maxTextNodeManage : 500; //Limit gesture for content text from element
	
		return this.each(function(index){		
			var $el = $(this);
			
			if(validInstallation($el)){
				initOptions($el);
				ellipsisManager($el);			
			}
			
		});		
		//Check if element is valid		
		function validInstallation($el){			
			var originalSource = $el.text();
			
			if($el.get(0).length == 0 || !originalSource.length || originalSource.length > MAX_TEXT_NODE_MANAGE){
				logToConsole('Element is not valid!');
				return false;
			}		
			if($el.data('mlinesEllipsis')){
				logToConsole('MLEllipsis plugin is already set on this element!!');
				return false;
			}
			
			return true;

		}
		//Init options for plugin on element
		function initOptions($el){	
			//Set data on element
			if(!$el.hasClass('mlEllipsisStyle'))
					$el	.addClass('mlEllipsisStyle');
			
			if(!isDefined($el.data('mlinesEllipsis'))){
				$el	.data('mlinesEllipsis', {
					codeID       		: mlinesEllipsisID++, //Increment codeID
					originalSource		: $el.text(), //Original Text
					ellipsisApplied		: false, //If ellipsis is applied
					_showWindowWidth	: $window.width(), //Width of window when plugin is set on this element
					_showWindowHeigth	: $window.height(),//Height of window when plugin is set on this element
					timeoutResize 		: 0 //Timeout for resize handler
				});
			}
		}
		
		function destroyElement($el){
				$el.get(0).parentNode.removeChild($el.get(0));
		}
		//Upadte multiline ellipsis called on resize window
		function updateEllipsis($el){
			var elementData = $el.data('mlinesEllipsis');		
							
			if(elementData.originalSource != $el.text()){
				$el.text($el.data('mlinesEllipsis').originalSource);
			}		
			
			initOptions($el);
			ellipsisManager($el);	
		}
		//Clean last chart id it's a space or tabulator
		function cleanLastCharts(str){
			var chartsManage = [' ', '	'];
			var i=0;
			do{	
				var lastChart = str.charAt(str.length-4);
				
				if(lastChart == chartsManage[i]){
					str = $.trim(str.substring(0, str.length-4))+'...';					
				}						
				else{
					i++;
				}
				
			}while(i<chartsManage.length);
			
			return str;
		}
		//Core for multilines ellipsis
		function ellipsisManager($el){
				var elementData = $el.data('mlinesEllipsis');					

				//Clone original element for secure operations
				var $cloneElement = $($el.get(0).cloneNode(true))
									 			.hide()
												.css('position', 'absolute')
												.css('overflow', 'visible')
												.width($el.width())
												.height('auto');


				//Append clone element after original element
				$el.after($cloneElement);

				var elHeight = $el.height(); //Heigth of original element
				
				//Manage the difference between height clone element and original element
				function height() { 
					return $cloneElement.outerHeight() > elHeight;
				};									                       
				//If difference between height clone element and original element is false
				if(!height()){
					$el.removeClass('mlEllipsisStyle');	//remove install plugin	
					destroyElement($cloneElement); //destroy temporary clone element
				}
				else{
					var originalSource = elementData.originalSource;
					elementData.ellipsisApplied = true; //Ellipsis is applied
					//Core of calculation
					while (height()){
						originalSource = originalSource.substr(0, originalSource.length -1);
						$cloneElement.text(originalSource+'...');
					}
					//Insert calculate ellipsis content in orginal element content	
					if(elementData.ellipsisApplied){
						$el.text(cleanLastCharts($cloneElement.text()));
						destroyElement($cloneElement); //Destroy temp clone element
					}
				}		
				
				resizeManager($el);	//Bind Handler resize on window			
		}
		
		//Manage multilines ellipsis on resize window
		function resizeManager($el){
			var elementData = $el.data('mlinesEllipsis');			

			if ( elementData.timeoutResize ){
				clearTimeout( elementData.timeoutResize );
			}
			
			var resizeHandlerCallback = function(){
					if ( elementData._showWindowWidth != $window.width() || elementData._showWindowHeigth != $window.height() ){
						elementData._showWindowWidth  = $window.width();
						elementData._showWindowHeigth = $window.height();
						clearTimeout(elementData.timeoutResize);
						elementData.timeoutResize=setTimeout(function(){
							updateEllipsis($el);
						},1000);
					}
			};
			
			$window	.off('resize.mlinesEllipsis'+elementData.codeID)					
					.on('resize.mlinesEllipsis'+elementData.codeID,function(event){	
							resizeHandlerCallback();			
					});
		}	
    };
})(jQuery);


(function($) {
    $.fn.longPress = function(callback, timeout) {
        var timer;
        var $this = $(this); 
        timeout = timeout || 400;

        $(this).on("touchstart MSPointerDown", function(event) {
        	$this.css("-webkit-user-select", "none");
        	$this.css("-webkit-touch-callout", "none");
			clearTimeout(timer);
            timer = setTimeout(function() { 
            	callback(event.originalEvent, $(event.target)); 
            }, timeout);
        });
        $(this).on("touchmove MSPointerMove", function(event) {
        	clearTimeout(timer);
        });
        $(document).on("touchend MSPointerUp", function() {
        	$this.css("-webkit-user-select", "auto");
            clearTimeout(timer);
        });
    };
})(jQuery);

(function($) {
    $.fn.handleSelection = function(objectsSelector, callback, onMove, timeout) {
        var bgColor = "#bcc690";
        var oldTgt = null;
        var oldTgtBgColor = null;
        var started = false;
        var isTimeout = false;
        var timeouting = null;

        $(this).longPress(function(event) {
    		var tgt = $(event.target).closest($(objectsSelector));
			if (tgt.find("div").length>0)
				return;
        	event.preventDefault();
        	started = true;
			if (oldTgt && oldTgt!=tgt)
				oldTgt.css("background-color", oldTgtBgColor);
			oldTgt = tgt;
			oldTgtBgColor = $(tgt).css("background-color");
			tgt.css("background-color", bgColor);
			timeouting = setTimeout(function(){
				isTimeout=true;
			}, timeout);
        }, 150);

        $(this).on("touchmove", function(event) {
        	clearTimeout(timeouting);
        	if (started) {
            	event.preventDefault();
            	var tgt = $(document.elementFromPoint(event.originalEvent.touches[0].clientX, event.originalEvent.touches[0].clientY)).closest($(objectsSelector));
    			if (oldTgt && oldTgt.attr("id")!=tgt.attr("id")) {
                	onMove(event);
    	        	isTimeout = false;
    				oldTgt.css("background-color", oldTgtBgColor);
    				$(this).data("oldTgtBgColor", $(tgt).css("background-color"));
    				oldTgt = tgt;
    				oldTgtBgColor = $(tgt).css("background-color");
    				tgt.css("background-color", bgColor);
    			}
            }
        });

        $(document).on("touchend", function(event) {
        	clearTimeout(timeouting);
        	if (started) {
	        	var tgt = oldTgt;
				if (oldTgt) {
					oldTgt.css("background-color", oldTgtBgColor);
				}
				oldTgt = tgt;
				oldTgtBgColor = $(tgt).css("background-color");
				if (!isTimeout)
					callback(event, tgt);
	        	started = false;
        	}
        });
    };
})(jQuery);


/** Ritorna il timestamp in formato numerico o in formato stringa 
 *  Ex. num : 1399643062466 
 *  	read : 2014-05-09 15:44:22  */
function getTimestamp(){
			var _n = new Date().getTime()
			,a = new Date(_n)
			,b = [
				a.getFullYear()
				,a.getMonth()+1
				,a.getDate()
				,a.getHours()
				,a.getMinutes()
				,a.getSeconds()
			]
			,_r = b[0] +'-'+ (b[1]<10?'0':'')+b[1] +'-'+ (b[2] < 10 ? '0' : '')+b[2] +' '+ (b[3]<10?'0':'')+b[3] +':'+ (b[4]<10?'0':'')+b[4] +':'+ (b[5]<10?'0':'')+b[5];
		return { num: _n, read: _r };
};

/**
 * View the widget filter image tooltip
 */
function viewWidgetFilterTooltip(currentNode, refObjId){
	var userTipOptions = getOptionsForCloseTooltip(false, null, 'filterWidgetQtip');
	var ctxObject = buildNewCtxObject();
	addDataToCtx(ctxObject, 'refObjId', refObjId);
	createJQTooltipWithDataProvider($(currentNode), LANGUAGES.getOriginalLabel("widgetFilterListTitle"), 'widgetFilterList', userTipOptions, getRasterCtxContainer(ctxObject), 320);
};

/**
 * Controlla che sia supportato il MutationObserver
 */
function mutationObserverSupported() {
	var mutationObserver = window.WebKitMutationObserver || window.MutationObserver || window.MozMutationObserver;
		
	return mutationObserver;
}

/**
 * Se reportViewId è definito esegue la funzione se è stata passata, se non è definito mostra l'errore e la funzione esterna chiamante in un alert.
 * 
 * @param reportViewId
 * @param callbackFunction
 */
function validateReportViewAndExecute(reportViewId, callbackFunction){
	
	if(reportViewId){
		if ( callbackFunction && typeof ( callbackFunction ) == "function" ) 
			callbackFunction(reportViewId); 
	}
	else{
		var callerFunctionName = (arguments.callee.caller.name) ? arguments.callee.caller.name : arguments.callee.caller.toString().match(/function ([^\(]+)/)[1];
		alerting(LANGUAGES.getLabelHTMLEncode("reportNotFoundInMashboardPage") + ' ' + callerFunctionName);
	}
}


/**
 * Funzione per il download file pdf in modo controllato per ambiente mobile e desktop. In particolare,
 * per ambienti mobile, gestisce l'apertura di un nuovo tab e il controllo sull'attivazione del blocco 
 * popup di sistema.
 *  
 * @param url
 */
function downloadExportPdfFile(url) {
	if( ENV_VARS.isOnMobileDevice() == true ) {
        var open = window.open(url);
        if (open == null || typeof(open)=='undefined')
            alert("Turn off your pop-up blocker!\n\nWe try to open the following url:\n"+url);
	} else {
		isExportDownloading = true;
		window.location.href=url;
	}
}

/**
 * Funzione per la preview di un file pdf in modo controllato per ambiente mobile e desktop. Esegue 
 * l'anteprima embedded solo per sistemi desktop e, per ambienti mobile, gestisce l'apertura di un nuovo
 * tab e il controllo sull'attivazione del blocco popup di sistema.
 *  
 * @param url
 */
function previewExportPdfFile(url) {
	if( ENV_VARS.isOnMobileDevice() == true ) {
        var open = window.open(url);
        if (open == null || typeof(open)=='undefined')
            alert("Turn off your pop-up blocker!\n\nWe try to open the following url:\n"+url);
	} else {
		viewPeviewExportPdf(url);
	}
}

function openDataActionsPageFromLogout(id){
	var link = $("#"+id).attr("uL");
	var funToCallOnClose = function() {
		return rootPage.closeDataActionsPage();
	} ;
	rootPage.jQDialogIframeMaxMin('jQPDataActionLogout', LANGUAGES.getLabelHTMLEncode("plTlpDataActions"), ENV_VARS.getValue("contextPath")+'/cassiopeaWeb/planning/'+link, '98%', '95%', funToCallOnClose, true, true, true, true, true, true);
	closeJQDialog('lockedPlanningProcess');
};

function enableLockCheckingConditions(obj, isCollaborative, isLockedByMe) {
	var container = $(obj).closest("table");
	var $checked = $( "input:checked", container )
	var lockEnabled = $checked.length>0;
	var viewEnabled = false;
	var oldCrashId = "";
	var crashCounter = 0;
	var crashTotal = 0;
	var $lockButton = $(".lockUraSL", $(container).closest(".InpSrcList-internalTable")).parent();
	var $viewButton = $(".viewUraSL", $(container).closest(".InpSrcList-internalTable")).parent();
	var lockCounterArr = new Array();
	var viewCounterArr = new Array();
	
	if (isLockedByMe) {

		lockEnabled = false;
		viewEnabled = false;

	} else {

		$checked.each(function(index) {
			var $this = $(this);
			var nodeInfo = $this.data("nodeinfo");
	
			if (nodeInfo.error || nodeInfo.saving || (nodeInfo.lock && !isCollaborative))
				lockEnabled = false;
			
			//per ogni URA selezionata ciclo l'elenco degli stati
			$.each(nodeInfo.statesInfo, function(index, stateInfo){	 
				if (stateInfo.pending)
					lockEnabled = false;
	
				if (stateInfo.grant == "EDITING" || stateInfo.grant == "VIEW") {
					viewCounterArr[index] = true;
				}
	
				if (stateInfo.grant == "EDITING")
					lockCounterArr["" + stateInfo.stateId] = parseInt("0" + lockCounterArr["" + stateInfo.stateId])+1;
			});
	
			//se ci sono ura in crash devono avere tutte lo stesso id
			if (oldCrashId != "" && oldCrashId != nodeInfo.crashInfo.crashID)
				lockEnabled = false;
			oldCrashId = nodeInfo.crashInfo.crashID;
			
			if (nodeInfo.crashInfo.crash) {
				crashCounter ++;
				crashTotal = nodeInfo.crashInfo.crashSize;
			}
	
		});
		
		//se tutte le ura selezionate hanno almeno uno stato con lo stesso id che sia EDITING
		if (lockEnabled) {
			var allInEdit = false;
			for (var key in lockCounterArr) {
				if (lockCounterArr[key] == $checked.length)
					allInEdit = true;
			}
			
			if (!allInEdit)
				lockEnabled = false;
	
			if (crashTotal != crashCounter)
				lockEnabled = false;
		}
		
		//se tutte le ura selezionate hanno almeno uno stato che sia VIEW
		if (viewCounterArr.length == $checked.length && $checked.length>0) 
			viewEnabled = true;
	}

	$lockButton.button( "option", "disabled", !lockEnabled );
	$viewButton.button( "option", "disabled", !viewEnabled );

}

function checkRadioButton(obj, container) {
	$("input", container).each(function() {
		$(this).prop("checked", false);
	});
	$(obj).prop("checked", true);
}

function isHtml5DateSupport(){
	var datefield = document.createElement("input");
	
	datefield.setAttribute("type", "date");
	
	//if browser doesn't support input type="date"
	if (datefield.type !== 'date'){ 
		return false;
	}
	
	return true;
}

/**
 * Author 	: 	Pagano Francesco
 * Date 	:	24.04.2015
 * Controlla che il parametro passato è una stringa e che non sia vuota
 * @param str : Stringa da controllare
 */
function isValidString(str){
	return (typeof str == 'string' && str != '');
}

/**
 * Author 	: 	Pagano Francesco
 * Date 	:	24.04.2015
 * Controlla che il parametro passato sua un'array
 * @param arr : array da controllare
 */
function isValidArray(arr){
	return (Object.prototype.toString.call(arr) === '[object Array]') && arr.length;
}

/**
 * Author 	: 	Pagano Francesco
 * Date 	:	24.04.2015
 * Controlla che il parametro passato sua un oggetto
 * @param obj : oggetto da controllare
 */
function isValidObject(obj){
	if(Object.prototype.toString.call(obj) === '[object Object]'){
		for(var key in obj){
			return true;
		}
	}
	return false;
}

/**
 * Author 	: 	Pagano Francesco
 * Date 	:	24.04.2015
 * Effettua il replace per posizione su tutta la stringa passata;  
 * @param str : Stringa da modificare
 * @param stringToSearch : Stringa da ricercare
 * @param stringToReplaced : Array con keys in ordine di sostituzione con cui effettuare il replace
 */
 
function replaceAllForPosition(str, stringToSearch, stringToReplaced) {
	if(isValidString(str) && isValidString(stringToSearch) && isValidArray(stringToReplaced)){
		return str.replace(new RegExp(stringToSearch,"g"), function(x,y) {
			return stringToReplaced[--y];
		});
	}
};

/**
 * Author 	: 	Pagano Francesco
 * Date 	:	27.04.2015
 * Controlla che l'elemento passato sia effettivamente un HTML ELement ;  
 * @param element : Elemento da controllare;
 */
function isElement(element){
	return (
			typeof HTMLElement === "object" ? element instanceof HTMLElement : 
				element && typeof element === "object" && element !== null && element.nodeType === 1 && typeof element.nodeName==="string"
	);
}

function invokeAngularService(serviceName){
	var $injector = typeof angular !== 'undefined' ? angular.element(document).injector() : null;
	
	if($injector && $injector.has(serviceName)){
		var service = $injector.get(serviceName);
		return angular.isDefined(service.$get) ? $injector.invoke(service.$get, service) : service;
	}
	return false;
}
/* funzione per la gestione della risoluzione del T#111125: URA visualizzate sempre in stato saving */
/* ricarica l'iframe dell'interfaccia standard contenente le ura in saving */
function reloadProcessStateDetails(owner, processID){
	if (owner == csMLO && rootPage.centrale && rootPage.centrale.centrale && rootPage.centrale.centrale.pStateDetails) {
		var pStateDetails = rootPage.centrale.centrale.$("#pStateDetails");
		if (pStateDetails.attr("processId") == processID)
			rootPage.centrale.centrale.pStateDetails.location.href=rootPage.centrale.centrale.pStateDetails.location.href;
	}
}

function getApiByElement(element) {

	if(!isValidObject(element)){
		return null;
	}
	
	var api = {};

	if(window.DSH_PAGE){

		switch (element.getType()) {
		//CrossTab e Styled Report
		case '117', '148':
			$.extend( true, api, new DcyDshCrosstabApi(element) );
			break;
			//Graph	
		case '115':
			$.extend( true, api, new DcyDshGraphApi(element) );
			break;
			//Msh
		case '121':
			$.extend( true, api, new DcyDshPageApi(element) );
			break;
			// ExecuteObject
		case '141':
			$.extend( true, api, new DcyDshExecuteObjectApi(element) );
			break;
			//Generic management
		default:
			break;
		}
	}
	else{
		//TODO : DA IMPLEMENTARE
		//Window generica
		//angular.extend(api.page, new DcyGenericPageApi(tmplInstance));
	}

	return api;
}

function getTinyMceLanguage(lang) {
	switch (lang) {
		case "en":
			return "";
		case "fr":
			return "fr_FR"; //francese
		case "pt":
			return "pt_PT"; //portoghese
		case "nb":
			return "nb_NO"; //norvegese
		case "bg":
			return "bg_BG"; //bulgaro
		case "hu":
			return "hu_HU"; //ungherese
		case "sl":
			return "sl_SI"; //sloveno
		case "sv":
			return "sv_SE"; //svedese
		case "zh":
			return "zh_CN"; //cinese
		case "is":
			return "is_IS"; //islandese
		case "th":
			return "th_TH"; //thailandese
	}
	return lang;
}

function showLoginMessage(jsonMessage) {
	$(document).ready(function(){
		JQAlertWithTmpl("jqDialogLoginMessage", getLabel('message'), "auto", "auto", 800, 600, null, false, jsonMessage, "loginMessage_BaseTMPL");
	});
	
}

function isEquivalent(a, b) {
    // Create arrays of property names
    var aProps = Object.getOwnPropertyNames(a);
    var bProps = Object.getOwnPropertyNames(b);

    // If number of properties is different,
    // objects are not equivalent
    if (aProps.length != bProps.length) {
        return false;
    }

    for (var i = 0; i < aProps.length; i++) {
        var propName = aProps[i];

        // If values of same property are not equal,
        // objects are not equivalent
        if (a[propName] !== b[propName]) {
            return false;
        }
    }

    // If we made it this far, objects
    // are considered equivalent
    return true;
}


function deleteCookieByName(cookieName){
	var date = new Date();
	document.cookie =  cookieName + "=; Path=/; Expires=" + date.toGMTString() + ";";
}

/**
 * Author 	: 	Christian Alessandro Tantalo
 * Date 	:	15.10.2015
 * Questa funzione deve essere bindata sulla pressione del solo pulsante ctrl 
 * e deve essere eseguita prima della pressione di ctrl-c.
 * Prepara un oggetto contenente il testo che deve essere copiato.
 * @param elements : è un array di array che corrispondono rispettivamente 
 * alle righe e alle colonne in modo da formattare il testo
 */

function prepareCopyToClipboard(elements)
{
	var selectionEnabled = $(document).isSelectionEnabled();
	if (!selectionEnabled)
		$(document).enableSelection();

	var curElem = document.activeElement;
	var $div = $("<div style='position:fixed;left:-1000px;' ></div>").appendTo($("body"));
	var $textarea = $("<textarea class='copyToClipboard'></textarea>").appendTo($div);

	$textarea.on("keyup", function(){
		$textarea.remove();
		$div.remove();
		if (!selectionEnabled)
			$(document).disableSelection();
		$(curElem).focus();
	})

	var str = "";
	for (var rows=0;rows<elements.length;rows++) {
		for (var col=0;col<elements[rows].length;col++)
			str += elements[rows][col] + " ";
		str += "\n";
	}
	$textarea.html(str);
	$textarea.focus();
	$textarea.select();
}
